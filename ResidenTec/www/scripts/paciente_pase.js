﻿/* LO QUE SIGUE
arreglar valores tas tad tam y fc guardados para cada paciente y cada control porque no funcionan
lo que si funciona es guardar control_n para paciente 1 y 2. hacer para el resto de los 10 pacientes usando viewpac1_butpressed etc etc.

falta que se guarde el control balance y agregar calculo en cada paso


*/

/*

PROBLEMAS

1) cuando se crea un control de ritmo diuretico, se deja vacio y se reloadea la pagina el array es null

*/


//variables

var control_n = 0;
var VIEW_PAC = 0;
var view_paciente = 0;

var diu_total = 0;

var check_diu_sum = 0;

var diu_actual_value = 0;

var oldtime = 0;
var newtime = 0;

var ritdiu_data_saved = 0;

var TAM1 = 0;

//cont
control_hemodinamico_AR1 = [5]
var control_n = 0;
var control_nb = 0;
var control_nc = 0;
var control_nd = 0; // control respi
var control_ne = 0;
var control_nf = 0;
var control_ng = 0;
var control_nh = 0;
var control_ni = 0;
var control_nj = 0;
var control_nx = 0;


//PROMPTS VALUES
var TASVALUE;
var TADVALUE;
var FCVALUE;

//arrays control hemodinamico

//paciente 1
var control_hemodinamico_AR1 = [5];
var control_hemodinamico_AR2 = [5];
var control_hemodinamico_AR3 = [5];
var control_hemodinamico_AR4 = [5];
var control_hemodinamico_AR5 = [5];
//paciente 2
var control_hemodinamico_AR1B = [4];
var control_hemodinamico_AR2B = [4];
var control_hemodinamico_AR3B = [4];
var control_hemodinamico_AR4B = [4];
var control_hemodinamico_AR5B = [4];

var control_hemodinamico_n = 0;

var has_cont_hemodinamico_values = false;

// arrays control ritmo diuretico

var control_ritmodiu_AR1 = [8];

// array balance

var control_balance_AR1 = [7];
control_balance_AR1[0] = 0;

var balance_vaso = 0;
var balance_baxter = 0;
var balance_baxter_extra = 0;

//array respiratorio

var control_respi = [2];

// control respiratorio click check
var click_check = false


//clock
var get_h = 0;
var get_m = 0;

function volver () {
    window.location.href = "home_pase.html";
}

function home () {
    window.location.href = "index.html";
}

//DEBUG
document.getElementById("debug_vercontrolAR").addEventListener("click", function () {
    console.log(control_hemodinamico_AR1B);
});

document.getElementById("clear_localstorage").addEventListener("click", function () {
    localStorage.clear();
});





// ONLOAD CONTROL RESPI VALUES   continuar

function onload_respi() {

    

    if (viewpac1_butpressed === true) {

        var loadnd = localStorage.getItem("save_controlnd_p1");

        if (loadnd === null) {
            console.log("null");
        } else if (loadnd !== null) {
            var loadnd2 = JSON.parse(loadnd);
            control_nd = loadnd2;

            console.log(control_nd);

            if (control_nd === 1) {
                create_respiratorio();
            } else if (control_nd === 2) {
                control_nd = 1;
            }
        }
      

        var load_fr = localStorage.getItem("save_fr_p1");

        if (load_fr === null) {
            console.log("is null");
        } else if (load_fr !== null) {

            var load_fr2 = JSON.parse(load_fr);
            control_respi = load_fr2;

                        
            if (control_respi[1] === null) {
                console.log("null");
            } else {

                if (document.getElementById("frvalue") === null) {
                    console.log("null");
                } else {
                    document.getElementById("frvalue").innerHTML = control_respi[1];
                    document.getElementById("satvalue").innerHTML = control_respi[2];
                }
               
            }
           
        }

   
       

    } else if (viewpac2_butpressed === true) {
        var load_fr = localStorage.getItem("save_fr_p2");
        var load_fr2 = JSON.parse(load_fr);
        control_respi = load_fr2;
    }

}


//no funca
function mon_respi () {

    

    if (document.getElementById("but_addfr") === null) {
        console.log("null");

    } else {
        document.getElementById("but_addfr").addEventListener("click", function () {
            control_respi[1] = prompt("Agregar Frecuencia Respiratoria");

            console.log(control_respi[1]);

            document.getElementById("frvalue").innerHTML = control_respi[1];

            if (viewpac1_butpressed === true) {
                var save_fr = control_respi;
                var save_fr2 = JSON.stringify(save_fr);
                localStorage.setItem("save_fr_p1", save_fr2);

            } else if (viewpac2_butpressed === true) {
                var save_fr = control_respi;
                var save_fr2 = JSON.stringify(save_fr);
                localStorage.setItem("save_fr_p2", save_fr2);
            }

        });
    }

    if (document.getElementById("but_addsat") === null) {
        console.log("null");
    } else {
        document.getElementById("but_addsat").addEventListener("click", function () {
            control_respi[2] = prompt("Agregar Saturacion de O2");

            document.getElementById("satvalue").innerHTML = control_respi[2];

            if (viewpac1_butpressed === true) {
                var save_fr = control_respi;
                var save_fr2 = JSON.stringify(save_fr);
                localStorage.setItem("save_fr_p1", save_fr2);

            } else if (viewpac2_butpressed === true) {
                var save_fr = control_respi;
                var save_fr2 = JSON.stringify(save_fr);
                localStorage.setItem("save_fr_p2", save_fr2);
            }


        });
    }

}





function load_controlnb_onload() {

    if (viewpac1_butpressed === true) {
        var load_controlnb = localStorage.getItem("save_controlnb_p1");
        var load_controlnb2 = JSON.parse(load_controlnb);
        control_nb = load_controlnb2;
    } else if (viewpac2_butpressed === true) {
        var load_controlnb = localStorage.getItem("save_controlnb_p2");
        var load_controlnb2 = JSON.parse(load_controlnb);
        control_nb = load_controlnb2;

    } else if (viewpac3_butpressed === true) {
        var load_controlnb = localStorage.getItem("save_controlnb_p3");
        var load_controlnb2 = JSON.parse(load_controlnb);
        control_nb = load_controlnb2;

    } else if (viewpac4_butpressed === true) {
        var load_controlnb = localStorage.getItem("save_controlnb_p4");
        var load_controlnb2 = JSON.parse(load_controlnb);
        control_nb = load_controlnb2;

    } else if (viewpac5_butpressed === true) {
        var load_controlnb = localStorage.getItem("save_controlnb_p5");
        var load_controlnb2 = JSON.parse(load_controlnb);
        control_nb = load_controlnb2;

    } else if (viewpac6_butpressed === true) {
        var load_controlnb = localStorage.getItem("save_controlnb_p6");
        var load_controlnb2 = JSON.parse(load_controlnb);
        control_nb = load_controlnb2;

    } else if (viewpac7_butpressed === true) {
        var load_controlnb = localStorage.getItem("save_controlnb_p7");
        var load_controlnb2 = JSON.parse(load_controlnb);
        control_nb = load_controlnb2;

    } else if (viewpac8_butpressed === true) {
        var load_controlnb = localStorage.getItem("save_controlnb_p8");
        var load_controlnb2 = JSON.parse(load_controlnb);
        control_nb = load_controlnb2;

    } else if (viewpac9_butpressed === true) {
        var load_controlnb = localStorage.getItem("save_controlnb_p9");
        var load_controlnb2 = JSON.parse(load_controlnb);
        control_nb = load_controlnb2;

    } else if (viewpac10_butpressed === true) {
        var load_controlnb = localStorage.getItem("save_controlnb_p10");
        var load_controlnb2 = JSON.parse(load_controlnb);
        control_nb = load_controlnb2;

    }

}

function load_controlnc_onload() {

    if (viewpac1_butpressed === true) {
        var load_controlnc = localStorage.getItem("save_controlnc_p1");
        var load_controlnc2 = JSON.parse(load_controlnc);
        control_nc = load_controlnc2;

        console.log("control_nc de paciente 1 cargado");
        console.log(control_nc);

    } else if (viewpac2_butpressed === true) {
        var load_controlnc = localStorage.getItem("save_controlnc_p2");
        var load_controlnc2 = JSON.parse(load_controlnc);
        control_nc = load_controlnc2;

        console.log("control_nc de paciente 2 cargado");
        console.log(control_nc);

    } else if (viewpac3_butpressed === true) {
        var load_controlnc = localStorage.getItem("save_controlnc_p3");
        var load_controlnc2 = JSON.parse(load_controlnc);
        control_nc = load_controlnc2;

        console.log("control_nc de paciente 2 cargado");
        console.log(control_nc);

    } else if (viewpac4_butpressed === true) {
        var load_controlnc = localStorage.getItem("save_controlnc_p4");
        var load_controlnc2 = JSON.parse(load_controlnc);
        control_nc = load_controlnc2;

        console.log("control_nc de paciente 2 cargado");
        console.log(control_nc);

    } else if (viewpac5_butpressed === true) {
        var load_controlnc = localStorage.getItem("save_controlnc_p5");
        var load_controlnc2 = JSON.parse(load_controlnc);
        control_nc = load_controlnc2;

        console.log("control_nc de paciente 2 cargado");
        console.log(control_nc);

    } else if (viewpac6_butpressed === true) {
        var load_controlnc = localStorage.getItem("save_controlnc_p6");
        var load_controlnc2 = JSON.parse(load_controlnc);
        control_nc = load_controlnc2;

        console.log("control_nc de paciente 2 cargado");
        console.log(control_nc);

    } else if (viewpac7_butpressed === true) {
        var load_controlnc = localStorage.getItem("save_controlnc_p7");
        var load_controlnc2 = JSON.parse(load_controlnc);
        control_nc = load_controlnc2;

        console.log("control_nc de paciente 2 cargado");
        console.log(control_nc);

    } else if (viewpac8_butpressed === true) {
        var load_controlnc = localStorage.getItem("save_controlnc_p8");
        var load_controlnc2 = JSON.parse(load_controlnc);
        control_nc = load_controlnc2;

        console.log("control_nc de paciente 2 cargado");
        console.log(control_nc);

    } else if (viewpac9_butpressed === true) {
        var load_controlnc = localStorage.getItem("save_controlnc_p9");
        var load_controlnc2 = JSON.parse(load_controlnc);
        control_nc = load_controlnc2;

        console.log("control_nc de paciente 2 cargado");
        console.log(control_nc);

    } else if (viewpac10_butpressed === true) {
        var load_controlnc = localStorage.getItem("save_controlnc_p10");
        var load_controlnc2 = JSON.parse(load_controlnc);
        control_nc = load_controlnc2;

        console.log("control_nc de paciente 2 cargado");
        console.log(control_nc);

    }


}

// ONLOAD
function ONLOADPACVIEW() {

   
 
    startTime();

    //cargar pacientes
    var load_Logp0 = localStorage.getItem("store logp0");
    var load_Logp0_b = JSON.parse(load_Logp0);
    Log_p0 = load_Logp0_b;
    console.log(load_Logp0_b);

    var load_Logp1 = localStorage.getItem("store logp1");
    var load_Logp1_b = JSON.parse(load_Logp1);
    Log_p1 = load_Logp1_b;
    console.log(load_Logp1_b);

    var load_Logp2 = localStorage.getItem("store logp2");
    var load_Logp2_b = JSON.parse(load_Logp2);
    Log_p2 = load_Logp2_b;
    console.log(load_Logp2_b);

    var load_Logp3 = localStorage.getItem("store logp3");
    var load_Logp3_b = JSON.parse(load_Logp3);
    Log_p3 = load_Logp3_b;
    console.log(load_Logp3_b);

    var load_Logp4 = localStorage.getItem("store logp4");
    var load_Logp4_b = JSON.parse(load_Logp4);
    Log_p4 = load_Logp4_b;
    console.log(load_Logp4_b);

    var load_Logp5 = localStorage.getItem("store logp5");
    var load_Logp5_b = JSON.parse(load_Logp5);
    Log_p5 = load_Logp5_b;
    console.log(load_Logp5_b);

    var load_Logp6 = localStorage.getItem("store logp6");
    var load_Logp6_b = JSON.parse(load_Logp6);
    Log_p6 = load_Logp6_b;
    console.log(load_Logp6_b);

    var load_Logp7 = localStorage.getItem("store logp7");
    var load_Logp7_b = JSON.parse(load_Logp7);
    Log_p7 = load_Logp7_b;
    console.log(load_Logp7_b);

    var load_Logp8 = localStorage.getItem("store logp8");
    var load_Logp8_b = JSON.parse(load_Logp8);
    Log_p8 = load_Logp8_b;
    console.log(load_Logp8_b);

    var load_Logp9 = localStorage.getItem("store logp9");
    var load_Logp9_b = JSON.parse(load_Logp9);
    Log_p9 = load_Logp9_b;
    console.log(load_Logp9_b);

    adddata_topage();

   

    //load stored control_n
    if (viewpac1_butpressed === true) {

        var load_controln = localStorage.getItem("strd_control_n");
        var load_controln2 = JSON.parse(load_controln);
        control_n = load_controln2;
        console.log(control_n + " control_n LOADED");

    } else if (viewpac2_butpressed === true) {

        var load_controln = localStorage.getItem("strd_control_n2");
        var load_controln2 = JSON.parse(load_controln);
        control_n = load_controln2;
        console.log(control_n + " control_n LOADED");

    } else if (viewpac3_butpressed === true) {

        var load_controln = localStorage.getItem("strd_control_n3");
        var load_controln2 = JSON.parse(load_controln);
        control_n = load_controln2;
        console.log(control_n + " control_n LOADED");

    } else if (viewpac4_butpressed === true) {

        var load_controln = localStorage.getItem("strd_control_n4");
        var load_controln2 = JSON.parse(load_controln);
        control_n = load_controln2;
        console.log(control_n + " control_n LOADED");

    } else if (viewpac5_butpressed === true) {

        var load_controln = localStorage.getItem("strd_control_n5");
        var load_controln2 = JSON.parse(load_controln);
        control_n = load_controln2;
        console.log(control_n + " control_n LOADED");

    } else if (viewpac6_butpressed === true) {

        var load_controln = localStorage.getItem("strd_control_n6");
        var load_controln2 = JSON.parse(load_controln);
        control_n = load_controln2;
        console.log(control_n + " control_n LOADED");

    } else if (viewpac7_butpressed === true) {

        var load_controln = localStorage.getItem("strd_control_n7");
        var load_controln2 = JSON.parse(load_controln);
        control_n = load_controln2;
        console.log(control_n + " control_n LOADED");

    } else if (viewpac8_butpressed === true) {

        var load_controln = localStorage.getItem("strd_control_n8");
        var load_controln2 = JSON.parse(load_controln);
        control_n = load_controln2;
        console.log(control_n + " control_n LOADED");

    } else if (viewpac9_butpressed === true) {

        var load_controln = localStorage.getItem("strd_control_n9");
        var load_controln2 = JSON.parse(load_controln);
        control_n = load_controln2;
        console.log(control_n + " control_n LOADED");

    } else if (viewpac10_butpressed === true) {

        var load_controln = localStorage.getItem("strd_control_n10");
        var load_controln2 = JSON.parse(load_controln);
        control_n = load_controln2;
        console.log(control_n + " control_n LOADED");

    } else {
        alert("load stored control_n error");
    }

    create_div_onload();

    viewing_patient();

    mon_tasbut1();
    mon_tadbut1();
    mon_fcbut1();

    mon_tasbut2();
    mon_tadbut2();
    mon_fcbut2();

    mon_tasbut3();
    mon_tadbut3();
    mon_fcbut3();

    mon_tasbut4();
    mon_tadbut4();
    mon_fcbut4();

    mon_tasbut5();
    mon_tadbut5();
    mon_fcbut5();

  

    load_controlnb_onload();
   // load_controlnc_onload();

    //LOAD BALANCE DATA  hay un error aca
  
    if (viewpac1_butpressed === true) {

        console.log(control_balance_AR1);

       

        var load_controlnc = localStorage.getItem("save_controlnc_p1");
        var load_controlnc2 = JSON.parse(load_controlnc);
        control_nc = load_controlnc2;

        console.log("control_nc de paciente 1 cargado");
        console.log(control_nc);


        var load_balance_AR1 = localStorage.getItem("save_balance_AR1_p1");
        var load_balance_AR12 = JSON.parse(load_balance_AR1);
        if (load_balance_AR12 === null) {
            console.log("array loaded is null");
        } else {
            control_balance_AR1 = load_balance_AR12;
        }



        if (control_balance_AR1 === null) {

            console.log(control_balance_AR1);
        }

       

        console.log(control_balance_AR1 + " LOADED");

        if (control_nc === 1 || control_nc === 2) {

            create_control_balance();

            if (control_balance_AR1 === null) {
                console.log("control balance AR 1 is null");
            } else {

                var vasoinput0 = control_balance_AR1[0];
                var vasoinput1 = +vasoinput0 / 200;
                document.getElementById("input_vaso").innerHTML = vasoinput1;

                var baxterinput0 = control_balance_AR1[1];
                var baxterinput1 = +baxterinput0 / 500;
                document.getElementById("input_baxter").innerHTML = baxterinput1;

                var baxterexinput0 = control_balance_AR1[2];
                var baxterexinput1 = +baxterexinput0 / 500;
                document.getElementById("input_baxter_extraplan").innerHTML = baxterexinput1;


                var caca1 = control_balance_AR1[4];
                var caca2 = +caca1 / 200;
                document.getElementById("input_caca").value = caca2;

                document.getElementById("input_diuresis").value = control_balance_AR1[3];

                document.getElementById("input_pesopaciente").value = control_balance_AR1[5];
                document.getElementById("bal_total").innerHTML = control_balance_AR1[6];


            } 


        }

    } else if (viewpac2_butpressed === true) {

        var load_controlnc = localStorage.getItem("save_controlnc_p2");
        var load_controlnc2 = JSON.parse(load_controlnc);
        control_nc = load_controlnc2;

        console.log("control_nc de paciente 2 cargado");
        console.log(control_nc);

        console.log("onload paciente 2")

        var load_balance_AR1 = localStorage.getItem("save_balance_AR1_p2");
        var load_balance_AR12 = JSON.parse(load_balance_AR1);
        if (load_balance_AR12 === null) {
            console.log("array loaded is null");
        } else {
            control_balance_AR1 = load_balance_AR12;
        }




        if (control_balance_AR1 === null) {

            console.log(control_balance_AR1);
        }


        console.log(control_balance_AR1 + " LOADED");

        if (control_nc === 1 || control_nc === 2) {

            create_control_balance();

            if (control_balance_AR1 === null) {
                console.log("control balance AR 1 is null");
            } else {

                var vasoinput0 = control_balance_AR1[0];
                var vasoinput1 = +vasoinput0 / 200;
                document.getElementById("input_vaso").innerHTML = vasoinput1;

                var baxterinput0 = control_balance_AR1[1];
                var baxterinput1 = +baxterinput0 / 500;
                document.getElementById("input_baxter").innerHTML = baxterinput1;

                var baxterexinput0 = control_balance_AR1[2];
                var baxterexinput1 = +baxterexinput0 / 500;
                document.getElementById("input_baxter_extraplan").innerHTML = baxterexinput1;


                var caca1 = control_balance_AR1[4];
                var caca2 = +caca1 / 200;
                document.getElementById("input_caca").value = caca2;

                document.getElementById("input_diuresis").value = control_balance_AR1[3];

                document.getElementById("input_pesopaciente").value = control_balance_AR1[5];
                document.getElementById("bal_total").innerHTML = control_balance_AR1[6];
            }
        }
    } else if (viewpac3_butpressed === true) {

        var load_controlnc = localStorage.getItem("save_controlnc_p3");
        var load_controlnc2 = JSON.parse(load_controlnc);
        control_nc = load_controlnc2;

        console.log("control_nc de paciente 2 cargado");
        console.log(control_nc);

        console.log("onload paciente 2")

        var load_balance_AR1 = localStorage.getItem("save_balance_AR1_p3");
        var load_balance_AR12 = JSON.parse(load_balance_AR1);
        if (load_balance_AR12 === null) {
            console.log("array loaded is null");
        } else {
            control_balance_AR1 = load_balance_AR12;
        }




        if (control_balance_AR1 === null) {

            console.log(control_balance_AR1);
        }


        console.log(control_balance_AR1 + " LOADED");

        if (control_nc === 1 || control_nc === 2) {

            create_control_balance();

            if (control_balance_AR1 === null) {
                console.log("control balance AR 1 is null");
            } else {

                var vasoinput0 = control_balance_AR1[0];
                var vasoinput1 = +vasoinput0 / 200;
                document.getElementById("input_vaso").innerHTML = vasoinput1;

                var baxterinput0 = control_balance_AR1[1];
                var baxterinput1 = +baxterinput0 / 500;
                document.getElementById("input_baxter").innerHTML = baxterinput1;

                var baxterexinput0 = control_balance_AR1[2];
                var baxterexinput1 = +baxterexinput0 / 500;
                document.getElementById("input_baxter_extraplan").innerHTML = baxterexinput1;


                var caca1 = control_balance_AR1[4];
                var caca2 = +caca1 / 200;
                document.getElementById("input_caca").value = caca2;

                document.getElementById("input_diuresis").value = control_balance_AR1[3];

                document.getElementById("input_pesopaciente").value = control_balance_AR1[5];
                document.getElementById("bal_total").innerHTML = control_balance_AR1[6];
            }
        }


    } else if (viewpac4_butpressed === true) {

        var load_controlnc = localStorage.getItem("save_controlnc_p4");
        var load_controlnc2 = JSON.parse(load_controlnc);
        control_nc = load_controlnc2;

        console.log("control_nc de paciente 2 cargado");
        console.log(control_nc);

        console.log("onload paciente 2")

        var load_balance_AR1 = localStorage.getItem("save_balance_AR1_p4");
        var load_balance_AR12 = JSON.parse(load_balance_AR1);
        if (load_balance_AR12 === null) {
            console.log("array loaded is null");
        } else {
            control_balance_AR1 = load_balance_AR12;
        }




        if (control_balance_AR1 === null) {

            console.log(control_balance_AR1);
        }


        console.log(control_balance_AR1 + " LOADED");

        if (control_nc === 1 || control_nc === 2) {

            create_control_balance();

            if (control_balance_AR1 === null) {
                console.log("control balance AR 1 is null");
            } else {

                var vasoinput0 = control_balance_AR1[0];
                var vasoinput1 = +vasoinput0 / 200;
                document.getElementById("input_vaso").innerHTML = vasoinput1;

                var baxterinput0 = control_balance_AR1[1];
                var baxterinput1 = +baxterinput0 / 500;
                document.getElementById("input_baxter").innerHTML = baxterinput1;

                var baxterexinput0 = control_balance_AR1[2];
                var baxterexinput1 = +baxterexinput0 / 500;
                document.getElementById("input_baxter_extraplan").innerHTML = baxterexinput1;


                var caca1 = control_balance_AR1[4];
                var caca2 = +caca1 / 200;
                document.getElementById("input_caca").value = caca2;

                document.getElementById("input_diuresis").value = control_balance_AR1[3];

                document.getElementById("input_pesopaciente").value = control_balance_AR1[5];
                document.getElementById("bal_total").innerHTML = control_balance_AR1[6];
            }
        }

    } else if (viewpac5_butpressed === true) {

        var load_controlnc = localStorage.getItem("save_controlnc_p5");
        var load_controlnc2 = JSON.parse(load_controlnc);
        control_nc = load_controlnc2;

        console.log("control_nc de paciente 2 cargado");
        console.log(control_nc);

        console.log("onload paciente 2")

        var load_balance_AR1 = localStorage.getItem("save_balance_AR1_p5");
        var load_balance_AR12 = JSON.parse(load_balance_AR1);
        if (load_balance_AR12 === null) {
            console.log("array loaded is null");
        } else {
            control_balance_AR1 = load_balance_AR12;
        }




        if (control_balance_AR1 === null) {

            console.log(control_balance_AR1);
        }


        console.log(control_balance_AR1 + " LOADED");

        if (control_nc === 1 || control_nc === 2) {

            create_control_balance();

            if (control_balance_AR1 === null) {
                console.log("control balance AR 1 is null");
            } else {

                var vasoinput0 = control_balance_AR1[0];
                var vasoinput1 = +vasoinput0 / 200;
                document.getElementById("input_vaso").innerHTML = vasoinput1;

                var baxterinput0 = control_balance_AR1[1];
                var baxterinput1 = +baxterinput0 / 500;
                document.getElementById("input_baxter").innerHTML = baxterinput1;

                var baxterexinput0 = control_balance_AR1[2];
                var baxterexinput1 = +baxterexinput0 / 500;
                document.getElementById("input_baxter_extraplan").innerHTML = baxterexinput1;


                var caca1 = control_balance_AR1[4];
                var caca2 = +caca1 / 200;
                document.getElementById("input_caca").value = caca2;

                document.getElementById("input_diuresis").value = control_balance_AR1[3];

                document.getElementById("input_pesopaciente").value = control_balance_AR1[5];
                document.getElementById("bal_total").innerHTML = control_balance_AR1[6];
            }
        }

    } else if (viewpac6_butpressed === true) {

        var load_controlnc = localStorage.getItem("save_controlnc_p6");
        var load_controlnc2 = JSON.parse(load_controlnc);
        control_nc = load_controlnc2;

        console.log("control_nc de paciente 2 cargado");
        console.log(control_nc);

        console.log("onload paciente 2")

        var load_balance_AR1 = localStorage.getItem("save_balance_AR1_p6");
        var load_balance_AR12 = JSON.parse(load_balance_AR1);
        if (load_balance_AR12 === null) {
            console.log("array loaded is null");
        } else {
            control_balance_AR1 = load_balance_AR12;
        }




        if (control_balance_AR1 === null) {

            console.log(control_balance_AR1);
        }


        console.log(control_balance_AR1 + " LOADED");

        if (control_nc === 1 || control_nc === 2) {

            create_control_balance();

            if (control_balance_AR1 === null) {
                console.log("control balance AR 1 is null");
            } else {

                var vasoinput0 = control_balance_AR1[0];
                var vasoinput1 = +vasoinput0 / 200;
                document.getElementById("input_vaso").innerHTML = vasoinput1;

                var baxterinput0 = control_balance_AR1[1];
                var baxterinput1 = +baxterinput0 / 500;
                document.getElementById("input_baxter").innerHTML = baxterinput1;

                var baxterexinput0 = control_balance_AR1[2];
                var baxterexinput1 = +baxterexinput0 / 500;
                document.getElementById("input_baxter_extraplan").innerHTML = baxterexinput1;


                var caca1 = control_balance_AR1[4];
                var caca2 = +caca1 / 200;
                document.getElementById("input_caca").value = caca2;

                document.getElementById("input_diuresis").value = control_balance_AR1[3];

                document.getElementById("input_pesopaciente").value = control_balance_AR1[5];
                document.getElementById("bal_total").innerHTML = control_balance_AR1[6];
            }
        }

    }
        
       
    mon_closebalance();
    
    //LOAD RITMO DIURETICO DATA

    //load_ritmodiu_inicdata();// load arrays
   

    if (control_nb === 1) {

        if (viewpac1_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritmodiu_p1");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {
            
            control_ritmodiu_AR1 = load_ritmodiu_p12;
            console.log(control_ritmodiu_AR1 + " LOADED");
            }


        } else if (viewpac2_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritmodiu_p2");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {

                control_ritmodiu_AR1 = load_ritmodiu_p12;
                console.log(control_ritmodiu_AR1 + " LOADED");
            }

        } else if (viewpac3_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritdiu_AR_p3");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {

                control_ritmodiu_AR1 = load_ritmodiu_p12;
                console.log(control_ritmodiu_AR1 + " LOADED");
            }       

        } else if (viewpac4_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritdiu_AR_p4");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {

                control_ritmodiu_AR1 = load_ritmodiu_p12;
                console.log(control_ritmodiu_AR1 + " LOADED");
            }

          

        } else if (viewpac5_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritdiu_AR_p5");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {

                control_ritmodiu_AR1 = load_ritmodiu_p12;
                console.log(control_ritmodiu_AR1 + " LOADED");
            }

     

        } else if (viewpac6_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritdiu_AR_p6");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {

                control_ritmodiu_AR1 = load_ritmodiu_p12;
                console.log(control_ritmodiu_AR1 + " LOADED");
            }

         

        } else if (viewpac7_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritdiu_AR_p7");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {

                control_ritmodiu_AR1 = load_ritmodiu_p12;
                console.log(control_ritmodiu_AR1 + " LOADED");
            }

     

        } else if (viewpac8_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritdiu_AR_p8");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {

                control_ritmodiu_AR1 = load_ritmodiu_p12;
                console.log(control_ritmodiu_AR1 + " LOADED");
            }

       

        } else if (viewpac9_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritdiu_AR_p9");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {

                control_ritmodiu_AR1 = load_ritmodiu_p12;
                console.log(control_ritmodiu_AR1 + " LOADED");
            }

         

        } else if (viewpac10_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritdiu_AR_p10");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {

                control_ritmodiu_AR1 = load_ritmodiu_p12;
                console.log(control_ritmodiu_AR1 + " LOADED");
            }

        

        } else {
            console.log("error");
            console.log("ERROR");
        }

        if (control_ritmodiu_AR1 === null) {
            console.log("ritmodiu es null");

        } 

        
    
        create_div_ritmodiu();

        if (control_ritmodiu_AR1 === null) {
            console.log("null");

        } else {
            document.getElementById("data_diu_inic").innerHTML = control_ritmodiu_AR1[0] + " ml";
            document.getElementById("data_time_inic").innerHTML = control_ritmodiu_AR1[1] + "hs" + " " + control_ritmodiu_AR1[2];
            document.getElementById("data_diu_total").innerHTML = control_ritmodiu_AR1[5] + " ml";
           

        }
     



        mon_ritmodiu_but1();
        mon_ritmodiu_but2();

        console.log(control_ritmodiu_AR1 + " este es el valor del array");

        if (control_ritmodiu_AR1[5]) {
            document.getElementById("data_diu_total").innerHTML = control_ritmodiu_AR1[5] + " ml";
        } else {
           
            document.getElementById("data_diu_total").innerHTML = control_ritmodiu_AR1[0] + " ml";
        }

        if (control_ritmodiu_AR1 === null) {
            document.getElementById("ritdiu").innerHTML = "";
        } else {
            document.getElementById("ritdiu").innerHTML = control_ritmodiu_AR1[7];
        } 


      /* if (control_ritmodiu_AR1 === null) {
            document.getElementById("data_diu_total").innerHTML = control_ritmodiu_AR1[0];
        } else {
            document.getElementById("data_diu_total").innerHTML = control_ritmodiu_AR1[5];
        }

        if (control_ritmodiu_AR1 === null) {
            document.getElementById("ritdiu").innerHTML = "";
        } else {
            document.getElementById("ritdiu").innerHTML = control_ritmodiu_AR1[7];
        } */


    } else if (control_nb === 2) {
        console.log("ACAAAAAA");

        if (viewpac1_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritmodiu_p1");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {

                control_ritmodiu_AR1 = load_ritmodiu_p12;
                console.log(control_ritmodiu_AR1 + " LOADED");
            }


        } else if (viewpac2_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritmodiu_p2");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {

                control_ritmodiu_AR1 = load_ritmodiu_p12;
                console.log(control_ritmodiu_AR1 + " LOADED");
            }

        } else if (viewpac3_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritdiu_AR_p3");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {

                control_ritmodiu_AR1 = load_ritmodiu_p12;
                console.log(control_ritmodiu_AR1 + " LOADED");
            }

        } else if (viewpac4_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritdiu_AR_p4");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {

                control_ritmodiu_AR1 = load_ritmodiu_p12;
                console.log(control_ritmodiu_AR1 + " LOADED");
            }



        } else if (viewpac5_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritdiu_AR_p5");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {

                control_ritmodiu_AR1 = load_ritmodiu_p12;
                console.log(control_ritmodiu_AR1 + " LOADED");
            }



        } else if (viewpac6_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritdiu_AR_p6");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {

                control_ritmodiu_AR1 = load_ritmodiu_p12;
                console.log(control_ritmodiu_AR1 + " LOADED");
            }



        } else if (viewpac7_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritdiu_AR_p7");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {

                control_ritmodiu_AR1 = load_ritmodiu_p12;
                console.log(control_ritmodiu_AR1 + " LOADED");
            }



        } else if (viewpac8_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritdiu_AR_p8");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {

                control_ritmodiu_AR1 = load_ritmodiu_p12;
                console.log(control_ritmodiu_AR1 + " LOADED");
            }



        } else if (viewpac9_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritdiu_AR_p9");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {

                control_ritmodiu_AR1 = load_ritmodiu_p12;
                console.log(control_ritmodiu_AR1 + " LOADED");
            }



        } else if (viewpac10_butpressed === true) {

            var load_ritmdiu_p1 = localStorage.getItem("save_ritdiu_AR_p10");
            var load_ritmodiu_p12 = JSON.parse(load_ritmdiu_p1);

            if (load_ritmodiu_p12 === null) {
                console.log("is null");
            } else {

                control_ritmodiu_AR1 = load_ritmodiu_p12;
                console.log(control_ritmodiu_AR1 + " LOADED");
            }



        } else {
            console.log("error");
            console.log("ERROR");
        }

        if (control_ritmodiu_AR1 === null) {
            console.log("ritmodiu es null");

        }



        create_div_ritmodiu();

        if (control_ritmodiu_AR1 === null) {
            console.log("null");

        } else {
            document.getElementById("data_diu_inic").innerHTML = control_ritmodiu_AR1[0] + " ml";
            document.getElementById("data_time_inic").innerHTML = control_ritmodiu_AR1[1] + "hs" + " " + control_ritmodiu_AR1[2];
            document.getElementById("data_diu_total").innerHTML = control_ritmodiu_AR1[5] + " ml";


        }




        mon_ritmodiu_but1();
        mon_ritmodiu_but2();

        console.log(control_ritmodiu_AR1 + " este es el valor del array");

        if (control_ritmodiu_AR1[5]) {
            document.getElementById("data_diu_total").innerHTML = control_ritmodiu_AR1[5] + " ml";
        } else {

            document.getElementById("data_diu_total").innerHTML = control_ritmodiu_AR1[0] + " ml";
        }

        if (control_ritmodiu_AR1 === null) {
            document.getElementById("ritdiu").innerHTML = "";
        } else {
            document.getElementById("ritdiu").innerHTML = control_ritmodiu_AR1[7];
        }


      /* if (control_ritmodiu_AR1 === null) {
            document.getElementById("data_diu_total").innerHTML = control_ritmodiu_AR1[0];
        } else {
            document.getElementById("data_diu_total").innerHTML = control_ritmodiu_AR1[5];
        }

        if (control_ritmodiu_AR1 === null) {
            document.getElementById("ritdiu").innerHTML = "";
        } else {
            document.getElementById("ritdiu").innerHTML = control_ritmodiu_AR1[7];
        } */

    }

    mon_closeritdiu();
    onload_respi();
    mon_respi();
}




function set_control_number_onclick() {  //runs when hemodinamico is clicked

    var addcontrol = control_n + 1;
    control_n = addcontrol;
    console.log("control number " + control_n);

    save_control_n();

    //alert maximo controles
    if (control_n === 5) {
        alert("maxima cantidad de controles");
    }

    create_div_HEMODINAMICO();

    mon_tasbut1();
    mon_tadbut1();
    mon_fcbut1();

    mon_tasbut2();
    mon_tadbut2();
    mon_fcbut2();

    mon_tasbut3();
    mon_tadbut3();
    mon_fcbut3();

    mon_tasbut4();
    mon_tadbut4();
    mon_fcbut4();

    mon_tasbut5();
    mon_tadbut5();
    mon_fcbut5();

    

    
}




//crea div de control hemodinamico
function create_div_HEMODINAMICO() {

    if (control_n <= 5 || control_n === null) {

    
    var div_controles = document.getElementById("controles");// get div to append

    var cr_bu1 = document.createElement("button");
    var cr_bu2 = document.createElement("button");
    var cr_bu3 = document.createElement("button");//FC

    var cr_div = document.createElement("div"); //create div
    var cr_div2 = document.createElement("div");
    var cr_div3 = document.createElement("div");
    var cr_div4 = document.createElement("div");//TAD titulo
    var cr_div5 = document.createElement("div");
    var cr_div6 = document.createElement("div");

    var cr_accordion = document.createElement("div");
    var cr_card = document.createElement("div");
    var cr_card_head = document.createElement("div");
    var cr_h5 = document.createElement("h5");
    var cr_head_but1 = document.createElement("button");
    var cr_div_collapse = document.createElement("div");
    var cr_cardbody = document.createElement("div");

    var cr_p = document.createElement("p"); // create p

    var cr_p_tas_tit = document.createElement("p");//TAS titulo
    var cr_p_tas_val = document.createElement("p");//TAS value 

    var cr_p_tad_tit = document.createElement("p");//TAD titulo
    var cr_p_tad_val = document.createElement("p");//TAD value

    var cr_p_tam_tit = document.createElement("p");//TAM titulo
    var cr_p_tam_value = document.createElement("p");//TAM value

    var cr_p_fc_tit = document.createElement("p");//FC titulo
    var cr_p_fc_value = document.createElement("p");//FC value

    //CONTROL N
    div_controles.appendChild(cr_div);//append div to controles div
    cr_div.setAttribute("class", "form-control");
    cr_div.setAttribute("id", "control nº" + control_n);// crear control number

    cr_div.appendChild(cr_accordion);
    cr_accordion.setAttribute("id", "accordionh" + control_n );

    cr_accordion.appendChild(cr_card);
    cr_card.setAttribute("class", "card");

    cr_card.appendChild(cr_card_head);
    cr_card_head.setAttribute("class", "card-header");
    cr_card_head.setAttribute("id", "headingThree");

    cr_card_head.appendChild(cr_h5);
    cr_h5.setAttribute("class", "mb-0");

    cr_h5.appendChild(cr_head_but1);
    cr_head_but1.setAttribute("class", "btn btn-primary collapsed");
    cr_head_but1.setAttribute("data-toggle", "collapse");
    cr_head_but1.setAttribute("data-target", "#collapseThree" + control_n);
    cr_head_but1.setAttribute("aria-expanded", "true");
    cr_head_but1.setAttribute("aria-controls", "collapseThree");
    cr_head_but1.innerHTML = "Hemodinamico";

    cr_card.appendChild(cr_div_collapse)
    cr_div_collapse.setAttribute("class", "collapse");
    cr_div_collapse.setAttribute("id", "collapseThree" + control_n);
    cr_div_collapse.setAttribute("aria-labelledby", "headingThree");
    cr_div_collapse.setAttribute("data-parent", "#accordionh" + control_n);

    cr_div_collapse.appendChild(cr_cardbody);
    cr_cardbody.setAttribute("class", "card-body");


   

    //TITULO
    cr_cardbody.appendChild(cr_div2);//div titulo
    cr_div2.setAttribute("id", "titulo" + control_n);
    cr_div2.setAttribute("class", "form-control h4 text-center");
    document.getElementById("titulo" + control_n).innerHTML = "Control Hemodinamico Nº " + control_n;


    //agregar TAS button
    document.getElementById("titulo" + control_n).appendChild(cr_div3);//TAS
    cr_div3.setAttribute("id", "div_TAS" + control_n);
    cr_div3.setAttribute("class", "form-control");
    cr_div3.appendChild(cr_bu1);
    cr_bu1.setAttribute("id", "addtas_bu" + control_n);
    cr_bu1.setAttribute("class", "btn btn-primary btn-lg btn-block");
    document.getElementById("addtas_bu" + control_n).innerHTML = "Agregar TAS";

    //TAS titulo
    cr_div3.appendChild(cr_p_tas_tit);
    cr_p_tas_tit.setAttribute("id", "P TAS" + control_n);
    cr_p_tas_tit.setAttribute("class", "h5");
    document.getElementById("P TAS" + control_n).innerHTML = "Tension Arterial Sistolica";

    //TAS value
    cr_div3.appendChild(cr_p_tas_val);
    cr_p_tas_val.setAttribute("id", "TASval" + control_n);
  
    document.getElementById("TASval" + control_n).innerHTML = "TAS?";

    //agregar TAD button
    document.getElementById("titulo" + control_n).appendChild(cr_div4);//TAD
    cr_div4.setAttribute("id", "div_TAD" + control_n);
    cr_div4.setAttribute("class", "form-control");
    cr_div4.appendChild(cr_bu2);
    cr_bu2.setAttribute("id", "addtad_bu" + control_n);
    cr_bu2.setAttribute("class", "btn btn-primary btn-lg btn-block");
    document.getElementById("addtad_bu" + control_n).innerHTML = "Agregar TAD";

    //TAD titulo
    cr_div4.appendChild(cr_p_tad_tit);
    cr_p_tad_tit.setAttribute("id", "P TAD" + control_n);
    cr_p_tad_tit.setAttribute("class", "h5");
    document.getElementById("P TAD" + control_n).innerHTML = "Tension Arterial Diastolica";

    //TAD value
    cr_div4.appendChild(cr_p_tad_val);
    cr_p_tad_val.setAttribute("id", "TADval" + control_n);

    document.getElementById("TADval" + control_n).innerHTML = "TAD?";

    //TAM titulo
    document.getElementById("titulo" + control_n).appendChild(cr_div5);
    cr_div5.appendChild(cr_p_tam_tit);
    cr_p_tam_tit.setAttribute("id", "tamtit" + control_n);
    cr_p_tam_tit.setAttribute("class", "h5");
    document.getElementById("tamtit" + control_n).innerHTML = "Tension Arterial Media";

    //TAM value
    document.getElementById("titulo" + control_n).appendChild(cr_div5);
    cr_div5.appendChild(cr_p_tam_value);
    cr_p_tam_value.setAttribute("id", "tamval" + control_n);
    cr_p_tam_value.setAttribute("class", "value");
    document.getElementById("tamval" + control_n).innerHTML = TAM1;

    //agregar FC button
    document.getElementById("titulo" + control_n).appendChild(cr_div6);
    cr_div6.appendChild(cr_bu3);
    cr_div6.setAttribute("class", "form-control");
    cr_bu3.setAttribute("id", "addfc_bu" + control_n);
    cr_bu3.setAttribute("class", "btn btn-info btn-lg btn-block")
    document.getElementById("addfc_bu" + control_n).innerHTML = "Agregar FC";

    //FC titulo
    document.getElementById("titulo" + control_n).appendChild(cr_div6);
    cr_div6.appendChild(cr_p_fc_tit);
    cr_p_fc_tit.setAttribute("id", "fctit" + control_n);
    cr_p_fc_tit.setAttribute("class", "h5");
    document.getElementById("fctit" + control_n).innerHTML = "Frecuencia Cardiaca";

    //FC value
    document.getElementById("titulo" + control_n).appendChild(cr_div6);
    cr_div6.appendChild(cr_p_fc_value);
    cr_p_fc_value.setAttribute("id", "fcval" + control_n);    
    document.getElementById("fcval" + control_n).innerHTML = "FC?";

    /*hora
    cr_div.appendChild(cr_p);
    cr_p.setAttribute("id", "time control " + control_n);
    cr_p.setAttribute("class", "h5 text-center");
    document.getElementById("time control " + control_n).innerHTML = "Hora: " + get_h + ":" + get_m;*/

    //cont_hem_toAR();// todo ok 

    }
}


//save control_n value to localstorage
function save_control_n() {

    if (viewpac1_butpressed === true){

    var save_contn = control_n;
    var save_contnb = JSON.stringify(save_contn);
    localStorage.setItem("strd_control_n", save_contnb);
    console.log("control_n saved");

    } else if (viewpac2_butpressed === true) {

        var save_contn = control_n;
        var save_contnb = JSON.stringify(save_contn);
        localStorage.setItem("strd_control_n2", save_contnb);
        console.log("control_n saved");

    } else if (viewpac3_butpressed === true) {

        var save_contn = control_n;
        var save_contnb = JSON.stringify(save_contn);
        localStorage.setItem("strd_control_n3", save_contnb);
        console.log("control_n saved");

    } else if (viewpac4_butpressed === true) {

        var save_contn = control_n;
        var save_contnb = JSON.stringify(save_contn);
        localStorage.setItem("strd_control_n4", save_contnb);
        console.log("control_n saved");

    } else if (viewpac5_butpressed === true) {

        var save_contn = control_n;
        var save_contnb = JSON.stringify(save_contn);
        localStorage.setItem("strd_control_n5", save_contnb);
        console.log("control_n saved");

    } else if (viewpac6_butpressed === true) {

        var save_contn = control_n;
        var save_contnb = JSON.stringify(save_contn);
        localStorage.setItem("strd_control_n6", save_contnb);
        console.log("control_n saved");

    } else if (viewpac7_butpressed === true) {

        var save_contn = control_n;
        var save_contnb = JSON.stringify(save_contn);
        localStorage.setItem("strd_control_n7", save_contnb);
        console.log("control_n saved");

    } else if (viewpac8_butpressed === true) {

        var save_contn = control_n;
        var save_contnb = JSON.stringify(save_contn);
        localStorage.setItem("strd_control_n8", save_contnb);
        console.log("control_n saved");

    } else if (viewpac9_butpressed === true) {

        var save_contn = control_n;
        var save_contnb = JSON.stringify(save_contn);
        localStorage.setItem("strd_control_n9", save_contnb);
        console.log("control_n saved");

    } else if (viewpac10_butpressed === true) {

        var save_contn = control_n;
        var save_contnb = JSON.stringify(save_contn);
        localStorage.setItem("strd_control_n10", save_contnb);
        console.log("control_n saved");

    } else {
        alert("save_control_n Error");
        console.log("ERROR");
    }
}





//cantidad de divs creados on loa ddependiendo del valor de control_n
function create_div_onload() {

    if (control_n === 1) {

        control_hemodinamico_AR1[0] = 1;
      
        create_div_HEMODINAMICO();

        console.log("div 1 created");


    } else if (control_n === 2) {

        var rest_controln = control_n - 1;
        control_n = rest_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();

    } else if (control_n === 3) {
        var rest_controln = control_n - 2;
        control_n = rest_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();
    } else if (control_n === 4) {
        var rest_controln = control_n - 3;
        control_n = rest_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();
    } else if (control_n === 5) {
        var rest_controln = control_n - 4;
        control_n = rest_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();

        var sum_controln = control_n + 1;
        control_n = sum_controln;

        create_div_HEMODINAMICO();
    }


}

// create div ritmo diuretico

function create_div_ritmodiu() {

    if (control_nb === 0 || control_nb === 1 || control_nb === 2) {
        var div_controles = document.getElementById("controles");// get div to append

        var cr_div = document.createElement("div"); //create div
        var cr_div_sum = document.createElement("div");
        var cr_div_data = document.createElement("div");
        var cr_div2 = document.createElement("div");
        var cr_div3 = document.createElement("div");
        var cr_div4 = document.createElement("div");
        var cr_div5 = document.createElement("div");
        var cr_div_cardbody2 = document.createElement("div");
        var cr_p = document.createElement("p");
        var cr_p2 = document.createElement("p");
        var cr_p3 = document.createElement("p");
        var cr_p4 = document.createElement("p");
        var cr_p5 = document.createElement("p");
        var cr_p6 = document.createElement("p");
        var cr_p7 = document.createElement("p");
        var cr_p8 = document.createElement("p");
        var cr_p9 = document.createElement("p");
        var cr_p10 = document.createElement("p");
        var cr_br1 = document.createElement("BR");
        var cr_br2 = document.createElement("BR");
        var cr_br3 = document.createElement("BR");
        var cr_br4 = document.createElement("BR");
        var cr_br5 = document.createElement("BR");
        var cr_br6 = document.createElement("BR");
        var cr_br7 = document.createElement("BR");
        var p_ritdiutit = document.createElement("p");
        var p_ritdiu = document.createElement("p");
        var cr_hr1 = document.createElement("HR");
        var cr_hr2 = document.createElement("HR");
        var cr_hr3 = document.createElement("HR");
        var cr_hr4 = document.createElement("HR");
        var cr_h51 = document.createElement("H5");

        var cr_sum_p1 = document.createElement("p");
        var cr_sum_p2 = document.createElement("p");//pending

        var cr_txt_input = document.createElement("INPUT");
        var cr_time_input = document.createElement("INPUT");
        var cr_date_input = document.createElement("INPUT");
        var cr_but1 = document.createElement("button");
        var cr_but_card1 = document.createElement("button");
        var cr_txt_input2 = document.createElement("INPUT");
        var cr_but_closeritdiu = document.createElement("button");
        var cr_but2 = document.createElement("button");
        var cr_row1 = document.createElement("div");
        var cr_col1 = document.createElement("div");
        var cr_col2 = document.createElement("div");
        var cr_col3 = document.createElement("div");

        div_controles.appendChild(cr_div2);
        cr_div2.setAttribute("id", "accordion2");
        
        cr_div2.appendChild(cr_div);//append div to controles div
        cr_div.setAttribute("class", "card");
        cr_div.setAttribute("id", "control_ritmodiuretico");// crear control number

        cr_div.appendChild(cr_row1);
        cr_row1.setAttribute("class", "row");

        cr_row1.appendChild(cr_col1);
        cr_col1.setAttribute("class", "col-sm");

        cr_row1.appendChild(cr_col2);
        cr_col2.setAttribute("class", "col-sm");

        cr_row1.appendChild(cr_col3);
        cr_col3.setAttribute("class", "col-sm");

        cr_col3.appendChild(cr_but_closeritdiu);
        cr_but_closeritdiu.setAttribute("class", "btn btn-secondary btn-sm float-right");
        cr_but_closeritdiu.setAttribute("id", "but_closeritmodiu");
        cr_but_closeritdiu.innerHTML = "x";

        cr_div.appendChild(cr_div4);
        cr_div4.setAttribute("class", "card-header");
        cr_div4.setAttribute("id", "headingTwo");

        cr_div4.appendChild(cr_h51);
        cr_h51.setAttribute("class", "mb-0");

        cr_h51.appendChild(cr_but_card1);
        cr_but_card1.setAttribute("class", "btn btn-primary collapsed");
        cr_but_card1.setAttribute("data-toggle", "collapse");
        cr_but_card1.setAttribute("data-target", "#collapseTwo");
        cr_but_card1.setAttribute("aria-expanded", "#false");
        cr_but_card1.setAttribute("aria-controls", "collapseTwo");
        cr_but_card1.setAttribute("style", "background:#46285b;")
        cr_but_card1.setAttribute("style", "border:#46285b;")
        cr_but_card1.innerHTML = "Ritmo Diuretico";

        cr_div.appendChild(cr_div5);
        cr_div5.setAttribute("class", "collapse show");
        cr_div5.setAttribute("id", "collapseTwo");
        cr_div5.setAttribute("aria-labelledby", "headingTwo");
        cr_div5.setAttribute("data-parent", "accordion2");

        cr_div5.appendChild(cr_div_cardbody2);
        cr_div_cardbody2.setAttribute("class", "card-body");


        //titulo
        cr_div_cardbody2.appendChild(cr_p);
        cr_p.setAttribute("id", "ritmodiu_titulo");
        cr_p.setAttribute("class", "h4 text-center");
        cr_p.innerHTML = "Ritmo Diuretico"

        //diuresis de inicio
        cr_div_cardbody2.appendChild(cr_p2);
        cr_p2.setAttribute("id", "diu_inicio");
        cr_p2.innerHTML = "Diuresis inicial (en ml)";
        cr_div_cardbody2.appendChild(cr_txt_input);
        cr_txt_input.setAttribute("id", "diu_inic_input");
        cr_txt_input.setAttribute("class", "form-control")
        cr_txt_input.setAttribute("placeholder", "Ingresar en mililitros");

        cr_div_cardbody2.appendChild(cr_br1);

        //time
        cr_div_cardbody2.appendChild(cr_p8);
        cr_p8.innerHTML = "Fecha de inicio";
        cr_div_cardbody2.appendChild(cr_date_input);
        cr_date_input.setAttribute("type", "date");
        cr_date_input.setAttribute("class", "form-control");
        cr_date_input.setAttribute("id", "date1");

        cr_div_cardbody2.appendChild(cr_br2);

        cr_div_cardbody2.appendChild(cr_p3);
        cr_p3.innerHTML = "Hora de inicio";
        cr_div_cardbody2.appendChild(cr_time_input);
        cr_time_input.setAttribute("id", "time1");
        cr_time_input.setAttribute("class", "form-control");
        cr_time_input.setAttribute("type", "time");
        cr_time_input.setAttribute("value", "00:00");

        cr_div_cardbody2.appendChild(cr_br3);

        //boton ingresar datos de inicio
        cr_div_cardbody2.appendChild(cr_but1);
        cr_but1.setAttribute("id", "ritdiu_inicio_but");
        cr_but1.setAttribute("class", "btn btn-success btn-lg btn-block");
        cr_but1.setAttribute("style", "background:#46285b;border-color:#46285b;")
        cr_but1.innerHTML = "Aceptar parametros de inicio";



        cr_div_cardbody2.appendChild(cr_br4);

        //DATA
        cr_div_cardbody2.appendChild(cr_div_data);
        cr_div_data.setAttribute("class", "card text-center");
        cr_div_data.appendChild(cr_p4);
        cr_p4.setAttribute("class", "h2");
        cr_p4.innerHTML = "Diuresis de Inicio: ";
        cr_div_data.appendChild(cr_p5);
        cr_p5.setAttribute("id", "data_diu_inic");
        cr_p5.setAttribute("class", "h3");

        cr_div_data.appendChild(cr_hr1);

        cr_div_data.appendChild(cr_p6);
        cr_p6.setAttribute("class", "h2");
        cr_p6.innerHTML = "Hora de Inicio";
        cr_div_data.appendChild(cr_p7);
        cr_p7.setAttribute("id", "data_time_inic");
        cr_p7.setAttribute("class", "h4");

        cr_div_data.appendChild(cr_hr2);

        cr_div_data.appendChild(cr_p9);
        cr_p9.setAttribute("class", "h1");
        cr_p9.innerHTML = "Diuresis Total";

        cr_div_data.appendChild(cr_hr3);

        cr_div_data.appendChild(cr_p10);
        cr_p10.setAttribute("class", "h2");
        cr_p10.setAttribute("id", "data_diu_total");

        //div suma
        cr_div_cardbody2.appendChild(cr_div_sum);
        cr_div_sum.setAttribute("id", "ritmodiu_control_sum");
        cr_div_sum.setAttribute("class", "card");

        cr_div_sum.appendChild(cr_br5);

        cr_div_sum.appendChild(cr_sum_p1);
        cr_sum_p1.innerHTML = "Ingresar control de diuresis actual (el valor ingresado se suma a la diuresis de ingreso)";

        cr_div_sum.appendChild(cr_txt_input2);
        cr_txt_input2.setAttribute("type", "text");
        cr_txt_input2.setAttribute("id", "diu_actual");
        cr_txt_input2.setAttribute("class", "form-control");
        cr_txt_input2.setAttribute("placeholder", "diuresis actual en ml");

        cr_div_sum.appendChild(cr_br6);

        cr_div_sum.appendChild(cr_but2);
        cr_but2.setAttribute("class", "btn btn-dark btn-lg btn-block")
        cr_but2.setAttribute("id", "diu_actual_but");
        cr_but2.innerHTML = "Sumar Diuresis Actual";

        cr_div_sum.appendChild(cr_br7);

        cr_div_sum.appendChild(p_ritdiutit);
        p_ritdiutit.setAttribute("class", "h3 text-center")
        p_ritdiutit.innerHTML = "Ritmo Diuretico";

        cr_div_sum.appendChild(cr_hr4);

        cr_div_sum.appendChild(p_ritdiu);
        p_ritdiu.setAttribute("class", "h3 text-center");
        p_ritdiu.setAttribute("id", "ritdiu");


    } else if (control_nb === 1) {
        alert("ya hay un control de ritmo diuretico");
    }
   
}



function mon_ritmodiu_but1() {

    var test_but = document.getElementById("control_ritmodiuretico");

 

    if (test_but) {

        
         document.getElementById("ritdiu_inicio_but").addEventListener("click", function () {

           
            control_ritmodiu_AR1[0] = diu_inic_input.value;
            control_ritmodiu_AR1[1] = time1.value;
            control_ritmodiu_AR1[2] = date1.value;
            

            diu_total = control_ritmodiu_AR1[0];

            console.log(diu_inic_input.value);
            console.log(time1.value);
            console.log("array " + control_ritmodiu_AR1);


            //add values to data de inicio
            document.getElementById("data_diu_inic").innerHTML = control_ritmodiu_AR1[0] + " ml";
            document.getElementById("data_time_inic").innerHTML = control_ritmodiu_AR1[1] + "hs " + control_ritmodiu_AR1[2];
            document.getElementById("data_diu_total").innerHTML = control_ritmodiu_AR1[0] + " ml";

            var init_time = control_ritmodiu_AR1[2] + " " + control_ritmodiu_AR1[1];
            var init_time_ms = Date.parse(init_time);
            //console.log(init_time);
            //console.log(init_time_ms);
            control_ritmodiu_AR1[3] = init_time_ms;

            var oldtime = init_time_ms;
            //console.log(oldtime);

            console.log(control_ritmodiu_AR1);

            


            /*var y = "2014-03-03 08:00";
            var x = Date.parse(y);
            var z = new Date(x);

            console.log(x);
            console.log(z);*/
           

            if (viewpac1_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritmodiu_p1", save_ritdiu_AR2);

            } else if (viewpac2_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritmodiu_p2", save_ritdiu_AR2);

            } else if (viewpac3_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritdiu_AR_p3", save_ritdiu_AR2);

            } else if (viewpac4_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritdiu_AR_p4", save_ritdiu_AR2);

            } else if (viewpac5_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritdiu_AR_p5", save_ritdiu_AR2);

            } else if (viewpac6_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritdiu_AR_p6", save_ritdiu_AR2);

            } else if (viewpac7_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritdiu_AR_p7", save_ritdiu_AR2);

            } else if (viewpac8_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritdiu_AR_p8", save_ritdiu_AR2);

            } else if (viewpac9_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritdiu_AR_p9", save_ritdiu_AR2);

            } else if (viewpac10_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritdiu_AR_p10", save_ritdiu_AR2);

            }

            });

        
    }

   

}

function mon_ritmodiu_but2() {

    var test_but = document.getElementById("diu_actual_but");

    if (test_but) {

        document.getElementById("diu_actual_but").addEventListener("click", function () {

            console.log("PRESSED");


            if (control_ritmodiu_AR1[5] === undefined || control_ritmodiu_AR1[5] === 0) {
                diu_total = control_ritmodiu_AR1[0];
            } else {
                diu_total = control_ritmodiu_AR1[5];
            }

          

            var diu_input_value = document.getElementById("diu_actual").value;
            var diu1 = +diu_total + +diu_input_value;
            diu_total = diu1;

            document.getElementById("data_diu_total").innerHTML = diu_total + " ml";

            control_ritmodiu_AR1[5] = diu_total;

            control_ritmodiu_AR1[6] = true;

       

            //add date

            var new_datetime = new Date();
            var time1 = new_datetime.getTime();
        

            //time difference

            var newtime = time1;

            oldtime = control_ritmodiu_AR1[3];
            console.log(oldtime);

            var diff = Math.abs(newtime - oldtime);
            console.log(diff);
            var horas_dif = +diff / 3600000;
            console.log(horas_dif);
  

            //mililistros hora

            var ml_hora = +diu_total / +horas_dif;
            console.log(ml_hora + " ml/hs");

            document.getElementById("ritdiu").innerHTML = ml_hora + " ml/hs";
            control_ritmodiu_AR1[7] = ml_hora;
        

            if (viewpac1_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritmodiu_p1", save_ritdiu_AR2);

            } else if (viewpac2_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritdiu_AR_p2", save_ritdiu_AR2);

            } else if (viewpac3_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritdiu_AR_p3", save_ritdiu_AR2);

            } else if (viewpac4_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritdiu_AR_p4", save_ritdiu_AR2);

            } else if (viewpac5_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritdiu_AR_p5", save_ritdiu_AR2);

            } else if (viewpac6_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritdiu_AR_p6", save_ritdiu_AR2);

            } else if (viewpac7_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritdiu_AR_p7", save_ritdiu_AR2);

            } else if (viewpac8_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritdiu_AR_p8", save_ritdiu_AR2);

            } else if (viewpac9_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritdiu_AR_p9", save_ritdiu_AR2);

            } else if (viewpac10_butpressed === true) {

                var save_ritdiu_AR = control_ritmodiu_AR1;
                var save_ritdiu_AR2 = JSON.stringify(save_ritdiu_AR);
                localStorage.setItem("save_ritdiu_AR_p10", save_ritdiu_AR2);

            }
                
        });
    }

  

}

// button close ritmodiu

function mon_closeritdiu() {

    if (document.getElementById("but_closeritmodiu") === null) {
        console.log("is null");
    } else {

        document.getElementById("but_closeritmodiu").addEventListener("click", function () {
            control_nb = 0;

            var get_div = document.getElementById("accordion2");
            var get_parent = document.getElementById("controles");

            get_parent.removeChild(get_div);

            if (viewpac1_butpressed === true) {
                var save_controlnb = control_nb;
                var save_controlnb2 = JSON.stringify(save_controlnb);
                localStorage.setItem("save_controlnb_p1", save_controlnb2);
            } else if (viewpac2_butpressed === true) {
                var save_controlnb = control_nb;
                var save_controlnb2 = JSON.stringify(save_controlnb);
                localStorage.setItem("save_controlnb_p2", save_controlnb2);
            } else if (viewpac3_butpressed === true) {
                var save_controlnb = control_nb;
                var save_controlnb2 = JSON.stringify(save_controlnb);
                localStorage.setItem("save_controlnb_p3", save_controlnb2);
            } else if (viewpac4_butpressed === true) {
                var save_controlnb = control_nb;
                var save_controlnb2 = JSON.stringify(save_controlnb);
                localStorage.setItem("save_controlnb_p4", save_controlnb2);
            } else if (viewpac5_butpressed === true) {
                var save_controlnb = control_nb;
                var save_controlnb2 = JSON.stringify(save_controlnb);
                localStorage.setItem("save_controlnb_p5", save_controlnb2);
            } else if (viewpac6_butpressed === true) {
                var save_controlnb = control_nb;
                var save_controlnb2 = JSON.stringify(save_controlnb);
                localStorage.setItem("save_controlnb_p6", save_controlnb2);
            } else if (viewpac7_butpressed === true) {
                var save_controlnb = control_nb;
                var save_controlnb2 = JSON.stringify(save_controlnb);
                localStorage.setItem("save_controlnb_p7", save_controlnb2);
            } else if (viewpac8_butpressed === true) {
                var save_controlnb = control_nb;
                var save_controlnb2 = JSON.stringify(save_controlnb);
                localStorage.setItem("save_controlnb_p8", save_controlnb2);
            } else if (viewpac9_butpressed === true) {
                var save_controlnb = control_nb;
                var save_controlnb2 = JSON.stringify(save_controlnb);
                localStorage.setItem("save_controlnb_p9", save_controlnb2);
            } else if (viewpac10_butpressed === true) {
                var save_controlnb = control_nb;
                var save_controlnb2 = JSON.stringify(save_controlnb);
                localStorage.setItem("save_controlnb_p10", save_controlnb2);
            }

           

          

        });

    }



}


function load_ritmodiu_inicdata() {

    if (viewpac1_butpressed === true) {
        var load_diuinic = localStorage.getItem("save_ritmodiu_p1");
        var load_diuinic2 = JSON.parse(load_diuinic);
        control_ritmodiu_AR1 = load_diuinic2;
        console.log(control_ritmodiu_AR1);
    } else if (viewpac2_butpressed === true) {
        var load_diuinic = localStorage.getItem("save_ritmodiu_p2");
        var load_diuinic2 = JSON.parse(load_diuinic);
        control_ritmodiu_AR1 = load_diuinic2;
        console.log(control_ritmodiu_AR1);

    }

}


function create_control_ritmodiu() {

    control_nb_sum();

    create_div_ritmodiu();


    mon_ritmodiu_but1();
    mon_ritmodiu_but2();

    mon_closeritdiu();

    console.log(control_nb);

    if (viewpac1_butpressed === true) {

        var save_controlnb = control_nb;
        var save_controlnb2 = JSON.stringify(save_controlnb);
        localStorage.setItem("save_controlnb_p1", save_controlnb2);
    } else if (viewpac2_butpressed === true) {

        var save_controlnb = control_nb;
        var save_controlnb2 = JSON.stringify(save_controlnb);
        localStorage.setItem("save_controlnb_p2", save_controlnb2);

    } else if (viewpac3_butpressed === true) {

        var save_controlnb = control_nb;
        var save_controlnb2 = JSON.stringify(save_controlnb);
        localStorage.setItem("save_controlnb_p3", save_controlnb2);

    } else if (viewpac4_butpressed === true) {

        var save_controlnb = control_nb;
        var save_controlnb2 = JSON.stringify(save_controlnb);
        localStorage.setItem("save_controlnb_p4", save_controlnb2);

    } else if (viewpac5_butpressed === true) {

        var save_controlnb = control_nb;
        var save_controlnb2 = JSON.stringify(save_controlnb);
        localStorage.setItem("save_controlnb_p5", save_controlnb2);

    } else if (viewpac6_butpressed === true) {

        var save_controlnb = control_nb;
        var save_controlnb2 = JSON.stringify(save_controlnb);
        localStorage.setItem("save_controlnb_p6", save_controlnb2);

    } else if (viewpac7_butpressed === true) {

        var save_controlnb = control_nb;
        var save_controlnb2 = JSON.stringify(save_controlnb);
        localStorage.setItem("save_controlnb_p7", save_controlnb2);

    } else if (viewpac8_butpressed === true) {

        var save_controlnb = control_nb;
        var save_controlnb2 = JSON.stringify(save_controlnb);
        localStorage.setItem("save_controlnb_p8", save_controlnb2);

    } else if (viewpac9_butpressed === true) {

        var save_controlnb = control_nb;
        var save_controlnb2 = JSON.stringify(save_controlnb);
        localStorage.setItem("save_controlnb_p9", save_controlnb2);

    } else if (viewpac10_butpressed === true) {

        var save_controlnb = control_nb;
        var save_controlnb2 = JSON.stringify(save_controlnb);
        localStorage.setItem("save_controlnb_p10", save_controlnb2);

    }

}

function control_nb_sum() {

    if (control_nb === null) {
        control_nb = 0;

        if (control_nb === 0 || control_nb === 1) {
            var sum = +control_nb + 1;
            control_nb = sum;
            console.log("controlnb suma es: " + control_nb);
        }

    } else if (control_nb === 0) {

        if (control_nb === 0 || control_nb === 1) {
            var sum = +control_nb + 1;
            control_nb = sum;
            console.log("controlnb suma es: " + control_nb);
        }

    } else if (control_nb === 1) {

        if (control_nb === 0 || control_nb === 1) {
            var sum = +control_nb + 1;
            control_nb = sum;
            console.log("controlnb suma es: " + control_nb);
        }

    } else if (control_nb === 2) {
        console.log("control_nb es: " + control_nb);

        //control_nb = 1;
    }


}



//sumar control_nc
function sum_controlnc_onclick() {
    if (control_nc === null) {
        control_nc = 0;

        if (control_nc === 0 || control_nc === 1) {
            var sum = +control_nc + 1;
            control_nc = sum;
            console.log("controlnc suma es: " + control_nc);
        }

    } else if (control_nc === 1) {
        var sum = +control_nc + 1;
        control_nc = sum;
        console.log("controlnc suma es: " + control_nc);
    
    } else if (control_nc === 2) {

        console.log("control_nc es: " + control_nc);
    } else if (control_nc === 0) {

        var sum = +control_nc + 1;
        control_nc = sum;
        console.log("controlnc suma es: " + control_nc);

    }

    console.log(control_nc + " sumcontrolnc")

}


// control balance ingresos y egresos


//press balance button

function balance_but_pressed() {

    console.log(control_nc);
        
    sum_controlnc_onclick();
    create_control_balance();
    mon_closebalance();
}

function create_control_balance() {

    if (control_nc === 0 || control_nc === null || control_nc === 1){

    var div_controles = document.getElementById("controles");

    var cr_div1 = document.createElement("div");
    var cr_div1b = document.createElement("div");
    var cr_div1c = document.createElement("div");
    var cr_div1d = document.createElement("div");
    var cr_div_cardbody1 = document.createElement("div");
    var cr_div2 = document.createElement("div");
    var cr_div3 = document.createElement("div");
    var cr_div4 = document.createElement("div");
    var cr_div_balance_tabla = document.createElement("div");
    var cr_div_row1 = document.createElement("div");
    var cr_div_col1 = document.createElement("div");
    var cr_div_col2 = document.createElement("div");
    var cr_div_col3 = document.createElement("div");
    var cr_p1 = document.createElement("p");
    var cr_p2 = document.createElement("p");
    var cr_p3 = document.createElement("p");
    var cr_p4 = document.createElement("p");
    var cr_p5 = document.createElement("p");
    var cr_p6 = document.createElement("p");
    var cr_p7 = document.createElement("p");
    var cr_p8 = document.createElement("p");
    var cr_p9 = document.createElement("p");
    var cr_p10 = document.createElement("p");
    var cr_closebut = document.createElement("button");
    var cr_p_valdata = document.createElement("p");
    var cr_hr1 = document.createElement("hr");
    var cr_hr2 = document.createElement("hr");
    var cr_hr3 = document.createElement("hr");
    var cr_but1 = document.createElement("BUTTON");
    var cr_but2 = document.createElement("BUTTON");
    var cr_but3 = document.createElement("BUTTON");
    var cr_but4 = document.createElement("BUTTON");
    var cr_but5 = document.createElement("BUTTON");
    var cr_but6 = document.createElement("BUTTON");
    var cr_but7 = document.createElement("BUTTON");
    var cr_but8 = document.createElement("BUTTON");
    var cr_but9 = document.createElement("BUTTON");
    var cr_but_acc1 = document.createElement("BUTTON");
    var cr_inputgroup1 = document.createElement("div");
    var cr_inputgroup2 = document.createElement("div");
    var cr_inputgroup3 = document.createElement("div");
    var cr_inputgroup4 = document.createElement("div");
    var cr_inputgroup5 = document.createElement("div");
    var cr_input1 = document.createElement("p");
    var cr_input2 = document.createElement("p");
    var cr_input3 = document.createElement("p");
    var cr_input4 = document.createElement("INPUT");
    var cr_input5 = document.createElement("INPUT");
    var cr_input6 = document.createElement("INPUT");
    var cr_foot1 = document.createElement("FOOTER");
    var cr_foot2 = document.createElement("FOOTER");
    var cr_foot3 = document.createElement("FOOTER");
    var cr_h51 = document.createElement("H5");
    var cr_br1 = document.createElement("BR");
    var cr_br2 = document.createElement("BR");


    div_controles.appendChild(cr_div1b);
    cr_div1b.setAttribute("id", "accordion");

    cr_div1.appendChild(cr_div_row1);
    cr_div_row1.setAttribute("class", "row");

    cr_div_row1.appendChild(cr_div_col1);
    cr_div_col1.setAttribute("class", "col-sm");

    cr_div_row1.appendChild(cr_div_col2);
    cr_div_col2.setAttribute("class", "col-sm");

    cr_div_row1.appendChild(cr_div_col3);
    cr_div_col3.setAttribute("class", "col-sm");

    cr_div_col3.appendChild(cr_closebut);
    cr_closebut.setAttribute("class", "btn btn-secondary btn-sm float-right");
    cr_closebut.setAttribute("id", "but_closebalance");
    cr_closebut.innerHTML = "x";

    cr_div1b.appendChild(cr_div1);
    cr_div1.setAttribute("class", "card");
    cr_div1.setAttribute("id", "main_card");

    cr_div1.appendChild(cr_div1c);
    cr_div1c.setAttribute("class", "card-header");
    cr_div1c.setAttribute("id", "headingOne");

    cr_div1c.appendChild(cr_h51);
    cr_h51.setAttribute("class", "mb-0");

    cr_div1c.appendChild(cr_but_acc1);
    cr_but_acc1.setAttribute("class", "btn btn-primary");
    cr_but_acc1.setAttribute("data-toggle", "collapse");
    cr_but_acc1.setAttribute("data-target", "#collapseOne");
    cr_but_acc1.setAttribute("aria-expanded", "true");
    cr_but_acc1.setAttribute("aria-controls", "collapseOne");
    cr_but_acc1.setAttribute("style", "background:#46285b;")
    cr_but_acc1.innerHTML = "Balance";

    cr_div1.appendChild(cr_div1d);
    cr_div1d.setAttribute("class", "collapse show");
    cr_div1d.setAttribute("id", "collapseOne");
    cr_div1d.setAttribute("aria-labelledby", "headingOne");
    cr_div1d.setAttribute("data-parent", "#accordion");

    cr_div1d.appendChild(cr_div_cardbody1);
    cr_div_cardbody1.setAttribute("class", "card-body");



    cr_div_cardbody1.appendChild(cr_p1);
    cr_p1.innerHTML = "Balance";
    cr_p1.setAttribute("class", "h3 text-center");

    cr_div_cardbody1.appendChild(cr_hr1);

    cr_div_cardbody1.appendChild(cr_div2);
    cr_div2.setAttribute("class", "form-control text-center");

    cr_div2.appendChild(cr_p2);
    cr_p2.innerHTML = "Ingresos";
    cr_p2.setAttribute("class", "h4");

    cr_div2.appendChild(cr_hr2);

    cr_div2.appendChild(cr_p3);
    cr_p3.setAttribute("class", "h5");
    cr_p3.innerHTML = "Via Oral";

    cr_div2.appendChild(cr_foot1);
    cr_foot1.setAttribute("class", "blockquote-footer")
    cr_foot1.innerHTML = "1 vaso de liquido (200 ml)"

    cr_div2.appendChild(cr_inputgroup1);
    cr_inputgroup1.setAttribute("class", "input-group");

    cr_inputgroup1.appendChild(cr_input1);
    cr_input1.setAttribute("class", "form-control");
    cr_input1.setAttribute("id", "input_vaso");

    cr_inputgroup1.appendChild(cr_but1);
    cr_but1.setAttribute("class", "btn btn-sm btn-outline-secondary");
    cr_but1.setAttribute("id", "but_agregarvaso");
    cr_but1.innerHTML = "Agregar Vasos";

    cr_inputgroup1.appendChild(cr_but2);
    cr_but2.setAttribute("class", "btn btn-sm btn-outline-secondary");
    cr_but2.setAttribute("id", "but_quitarvaso");
    cr_but2.innerHTML = "Quitar Vasos";

    cr_div2.appendChild(cr_p4);
    cr_p4.setAttribute("class", "h5");
    cr_p4.innerHTML = "Endovenoso";

    cr_div2.appendChild(cr_p5);
    cr_p5.innerHTML = "PHP";

    cr_div2.appendChild(cr_foot2);
    cr_foot2.setAttribute("class", "blockquote-footer")
    cr_foot2.innerHTML = "1 baxter = 500 ml";

    cr_div2.appendChild(cr_inputgroup2);
    cr_inputgroup2.setAttribute("class", "input-group");

    cr_inputgroup2.appendChild(cr_input2);
    cr_input2.setAttribute("class", "form-control");
    cr_input2.setAttribute("id", "input_baxter");

    cr_inputgroup2.appendChild(cr_but3);
    cr_but3.setAttribute("class", "btn btn-sm btn-outline-secondary");
    cr_but3.setAttribute("id", "but_agregarbaxter");
    cr_but3.innerHTML = "Agregar Baxter";

    cr_inputgroup2.appendChild(cr_but4);
    cr_but4.setAttribute("class", "btn btn-sm btn-outline-secondary");
    cr_but4.setAttribute("id", "but_quitarbaxter");
    cr_but4.innerHTML = "Quitar Baxter";

    cr_div2.appendChild(cr_p6);
    cr_p6.innerHTML = "Paralelo";

    cr_div2.appendChild(cr_foot3);
    cr_foot3.setAttribute("class", "blockquote-footer")
    cr_foot3.innerHTML = "Extra plan (analgesia, antibioticos)";

    cr_div2.appendChild(cr_inputgroup3);
    cr_inputgroup3.setAttribute("class", "input-group");

    cr_inputgroup3.appendChild(cr_input3);
    cr_input3.setAttribute("class", "form-control");
    cr_input3.setAttribute("id", "input_baxter_extraplan");

    cr_inputgroup3.appendChild(cr_but5);
    cr_but5.setAttribute("class", "btn btn-sm btn-outline-secondary");
    cr_but5.setAttribute("id", "but_agregarbaxter_extraplan");
    cr_but5.innerHTML = "Agregar Baxter";

    cr_inputgroup3.appendChild(cr_but6);
    cr_but6.setAttribute("class", "btn btn-sm btn-outline-secondary");
    cr_but6.setAttribute("id", "but_quitarbaxter_extraplan");
    cr_but6.innerHTML = "Quitar Baxter";

    cr_div_cardbody1.appendChild(cr_div3);
    cr_div3.setAttribute("class", "form-control text-center");

    cr_div3.appendChild(cr_p7);
    cr_p7.setAttribute("class", "h4");
    cr_p7.innerHTML = "Egresos";

    cr_div3.appendChild(cr_hr3);

    cr_div3.appendChild(cr_p8);
    cr_p8.setAttribute("class", "h5");
    cr_p8.innerHTML = "Diuresis";

    cr_div3.appendChild(cr_inputgroup4);
    cr_inputgroup4.setAttribute("class", "input-group");

    cr_inputgroup4.appendChild(cr_input4);
    cr_input4.setAttribute("class", "form-control");
    cr_input4.setAttribute("id", "input_diuresis");

    cr_inputgroup4.appendChild(cr_but7);
    cr_but7.setAttribute("class", "btn btn-sm btn-outline-secondary");
    cr_but7.setAttribute("id", "but_diuresis");
    cr_but7.innerHTML = "Agregar Diuresis";

    cr_div3.appendChild(cr_br1);

    cr_div3.appendChild(cr_p9);
    cr_p9.setAttribute("class", "h5");
    cr_p9.innerHTML = "Deposiciones";

    cr_div3.appendChild(cr_inputgroup5);
    cr_inputgroup5.setAttribute("class", "input-group");

    cr_inputgroup5.appendChild(cr_input5);
    cr_input5.setAttribute("class", "form-control");
    cr_input5.setAttribute("id", "input_caca");

    cr_inputgroup5.appendChild(cr_but8);
    cr_but8.setAttribute("class", "btn btn-sm btn-outline-secondary");
    cr_but8.setAttribute("id", "but_agregardeposicion");
    cr_but8.innerHTML = "Agregar";

    cr_div_cardbody1.appendChild(cr_div4);
    cr_div4.setAttribute("class", "form-control text-center");

    cr_div4.appendChild(cr_p10);
    cr_p10.setAttribute("class", "h5");
    cr_p10.innerHTML = "Peso del Paciente";

    cr_div4.appendChild(cr_input6);
    cr_input6.setAttribute("id", "input_pesopaciente");
    cr_input6.setAttribute("class", "form-control");

    cr_div4.appendChild(cr_but9);
    cr_but9.setAttribute("id", "but_agregarpeso");
    cr_but9.setAttribute("class", "btn btn-lg btn-secondary");
    cr_but9.innerHTML = "Agregar y Finalizar Balance";

    div_controles.appendChild(cr_div_balance_tabla);
    cr_div_balance_tabla.setAttribute("class", "form-control");
    cr_div_balance_tabla.setAttribute("id", "balance_tabla");

    cr_div_balance_tabla.appendChild(cr_p_valdata);
    cr_p_valdata.setAttribute("class", "h2 text-center");
    cr_p_valdata.setAttribute("id", "bal_total");

    if (viewpac1_butpressed === true) {
        var save_controlnc = control_nc;
        var save_controlnc2 = JSON.stringify(save_controlnc);
        localStorage.setItem("save_controlnc_p1", save_controlnc2);
    } else if (viewpac2_butpressed === true) {
        var save_controlnc = control_nc;
        var save_controlnc2 = JSON.stringify(save_controlnc);
        localStorage.setItem("save_controlnc_p2", save_controlnc2);
    } else if (viewpac3_butpressed === true) {
        var save_controlnc = control_nc;
        var save_controlnc2 = JSON.stringify(save_controlnc);
        localStorage.setItem("save_controlnc_p3", save_controlnc2);
    } else if (viewpac4_butpressed === true) {
        var save_controlnc = control_nc;
        var save_controlnc2 = JSON.stringify(save_controlnc);
        localStorage.setItem("save_controlnc_p4", save_controlnc2);
    } else if (viewpac5_butpressed === true) {
        var save_controlnc = control_nc;
        var save_controlnc2 = JSON.stringify(save_controlnc);
        localStorage.setItem("save_controlnc_p5", save_controlnc2);
    } else if (viewpac6_butpressed === true) {
        var save_controlnc = control_nc;
        var save_controlnc2 = JSON.stringify(save_controlnc);
        localStorage.setItem("save_controlnc_p6", save_controlnc2);
    } else if (viewpac7_butpressed === true) {
        var save_controlnc = control_nc;
        var save_controlnc2 = JSON.stringify(save_controlnc);
        localStorage.setItem("save_controlnc_p7", save_controlnc2);
    } else if (viewpac8_butpressed === true) {
        var save_controlnc = control_nc;
        var save_controlnc2 = JSON.stringify(save_controlnc);
        localStorage.setItem("save_controlnc_p8", save_controlnc2);
    } else if (viewpac9_butpressed === true) {
        var save_controlnc = control_nc;
        var save_controlnc2 = JSON.stringify(save_controlnc);
        localStorage.setItem("save_controlnc_p9", save_controlnc2);
    } else if (viewpac10_butpressed === true) {
        var save_controlnc = control_nc;
        var save_controlnc2 = JSON.stringify(save_controlnc);
        localStorage.setItem("save_controlnc_p10", save_controlnc2);
    }

 
    

    } else if (control_nc === 2) {     
        console.log("ya hay un control de balance");

        control_nc = 1;
    }

    mon_balance();
   
}

// balance functions

function mon_closebalance() {

    if (document.getElementById("but_closebalance") === null) {
        console.log("is null");
    } else {

        document.getElementById("but_closebalance").addEventListener("click", function () {

            var cr_div1b = document.getElementById("controles");
            var cr_div1 = document.getElementById("accordion");
            var balance_tabla = document.getElementById("balance_tabla");

            console.log("cerrar");
            cr_div1b.removeChild(cr_div1);
            cr_div1b.removeChild(balance_tabla);

            control_nc = 0;

            if (viewpac1_butpressed === true) {           
                var save_controlnc = control_nc;
                var save_controlnc2 = JSON.stringify(save_controlnc);
                localStorage.setItem("save_controlnc_p1", save_controlnc2);


            } else if (viewpac2_butpressed === true) {
                var save_controlnc = control_nc;
                var save_controlnc2 = JSON.stringify(save_controlnc);
                localStorage.setItem("save_controlnc_p2", save_controlnc2);

            } else if (viewpac3_butpressed === true) {
                var save_controlnc = control_nc;
                var save_controlnc2 = JSON.stringify(save_controlnc);
                localStorage.setItem("save_controlnc_p3", save_controlnc2);

            } else if (viewpac4_butpressed === true) {
                var save_controlnc = control_nc;
                var save_controlnc2 = JSON.stringify(save_controlnc);
                localStorage.setItem("save_controlnc_p4", save_controlnc2);

            } else if (viewpac5_butpressed === true) {
                var save_controlnc = control_nc;
                var save_controlnc2 = JSON.stringify(save_controlnc);
                localStorage.setItem("save_controlnc_p5", save_controlnc2);

            } else if (viewpac6_butpressed === true) {
                var save_controlnc = control_nc;
                var save_controlnc2 = JSON.stringify(save_controlnc);
                localStorage.setItem("save_controlnc_p6", save_controlnc2);

            } else if (viewpac7_butpressed === true) {
                var save_controlnc = control_nc;
                var save_controlnc2 = JSON.stringify(save_controlnc);
                localStorage.setItem("save_controlnc_p7", save_controlnc2);

            } else if (viewpac8_butpressed === true) {
                var save_controlnc = control_nc;
                var save_controlnc2 = JSON.stringify(save_controlnc);
                localStorage.setItem("save_controlnc_p8", save_controlnc2);

            } else if (viewpac9_butpressed === true) {
                var save_controlnc = control_nc;
                var save_controlnc2 = JSON.stringify(save_controlnc);
                localStorage.setItem("save_controlnc_p9", save_controlnc2);

            } else if (viewpac10_butpressed === true) {
                var save_controlnc = control_nc;
                var save_controlnc2 = JSON.stringify(save_controlnc);
                localStorage.setItem("save_controlnc_p10", save_controlnc2);

            } 

            console.log(control_nc + "mirar aca");
           
            control_balance_AR1[0] = 0;
            control_balance_AR1[1] = 0;
            control_balance_AR1[2] = 0;
            control_balance_AR1[3] = 0;
            control_balance_AR1[4] = 0;
            control_balance_AR1[5] = 0;
            control_balance_AR1[6] = 0;
            control_balance_AR1[7] = 0;


            if (viewpac1_butpressed === true) {

                var save_balance_AR1 = control_balance_AR1;
                var save_balance_AR12 = JSON.stringify(save_balance_AR1);
                localStorage.setItem("save_balance_AR1_p1", save_balance_AR12);

            } else if (viewpac2_butpressed === true) {

                var save_balance_AR1 = control_balance_AR1;
                var save_balance_AR12 = JSON.stringify(save_balance_AR1);
                localStorage.setItem("save_balance_AR1_p2", save_balance_AR12);

            } else if (viewpac3_butpressed === true) {

                var save_balance_AR1 = control_balance_AR1;
                var save_balance_AR12 = JSON.stringify(save_balance_AR1);
                localStorage.setItem("save_balance_AR1_p3", save_balance_AR12);

            } else if (viewpac4_butpressed === true) {

                var save_balance_AR1 = control_balance_AR1;
                var save_balance_AR12 = JSON.stringify(save_balance_AR1);
                localStorage.setItem("save_balance_AR1_p4", save_balance_AR12);

            } else if (viewpac5_butpressed === true) {

                var save_balance_AR1 = control_balance_AR1;
                var save_balance_AR12 = JSON.stringify(save_balance_AR1);
                localStorage.setItem("save_balance_AR1_p5", save_balance_AR12);

            } else if (viewpac6_butpressed === true) {

                var save_balance_AR1 = control_balance_AR1;
                var save_balance_AR12 = JSON.stringify(save_balance_AR1);
                localStorage.setItem("save_balance_AR1_p6", save_balance_AR12);

            } else if (viewpac7_butpressed === true) {

                var save_balance_AR1 = control_balance_AR1;
                var save_balance_AR12 = JSON.stringify(save_balance_AR1);
                localStorage.setItem("save_balance_AR1_p7", save_balance_AR12);

            } else if (viewpac8_butpressed === true) {

                var save_balance_AR1 = control_balance_AR1;
                var save_balance_AR12 = JSON.stringify(save_balance_AR1);
                localStorage.setItem("save_balance_AR1_p8", save_balance_AR12);

            } else if (viewpac9_butpressed === true) {

                var save_balance_AR1 = control_balance_AR1;
                var save_balance_AR12 = JSON.stringify(save_balance_AR1);
                localStorage.setItem("save_balance_AR1_p9", save_balance_AR12);

            } else if (viewpac10_butpressed === true) {

                var save_balance_AR1 = control_balance_AR1;
                var save_balance_AR12 = JSON.stringify(save_balance_AR1);
                localStorage.setItem("save_balance_AR1_p10", save_balance_AR12);

            }

        });

    }

 
}

function mon_balance() {


    if (document.getElementById("but_agregarvaso") === null) {
        console.log("null");

    } else {

        document.getElementById("but_agregarvaso").addEventListener("click", function () {

           /* balance_vaso = control_balance_AR1[0];
            console.log("balance vaso is: " + balance_vaso);*/

            var sum_vaso = +balance_vaso + 1;
            balance_vaso = sum_vaso;
            console.log("balance vaso es: " + balance_vaso);

            document.getElementById("input_vaso").innerHTML = balance_vaso;

            var multiplier = balance_vaso;
            console.log(multiplier + "m");

            var vaso_to_ml = 200;

            var vo_ml = +vaso_to_ml * +multiplier;


            control_balance_AR1[0] = vo_ml;

            console.log(control_balance_AR1[0]);
            

        });


    }

    document.getElementById("but_quitarvaso").addEventListener("click", function () {

        if (balance_vaso === 0) {
            console.log("nada");
        } else {


        var sum_vaso = +balance_vaso - 1;
        balance_vaso = sum_vaso;

        document.getElementById("input_vaso").innerHTML = balance_vaso;

        var multiplier = balance_vaso;
        console.log(multiplier + "m");

        var vaso_to_ml = 200;

        var vo_ml = +vaso_to_ml * +multiplier;


        control_balance_AR1[0] = vo_ml;
        }


    });




    document.getElementById("but_agregarbaxter").addEventListener("click", function () {


        var sum_baxter = +balance_baxter + 1;
        balance_baxter = sum_baxter;
        console.log("balance vaso es: " + balance_baxter);

        document.getElementById("input_baxter").innerHTML = balance_baxter;

        var multiplier = balance_baxter;

        var baxter_to_ml = 500;

        var baxter_ml = +baxter_to_ml * +multiplier;


        control_balance_AR1[1] = baxter_ml;

        console.log(control_balance_AR1[1]);

    });

    document.getElementById("but_quitarbaxter").addEventListener("click", function () {

        if (balance_baxter === 0) {
            console.log("nada");
        } else {

        var sum_baxter = +balance_baxter - 1;
        balance_baxter = sum_baxter;
        console.log("balance vaso es: " + balance_baxter);

        document.getElementById("input_baxter").innerHTML = balance_baxter;

        var multiplier = balance_baxter;

        var baxter_to_ml = 500;

        var baxter_ml = +baxter_to_ml * +multiplier;


        control_balance_AR1[1] = baxter_ml;

        console.log(control_balance_AR1[1]);
        }

    });

    document.getElementById("but_agregarbaxter_extraplan").addEventListener("click", function () {

        var sum_baxter_extra = +balance_baxter_extra + 1;
        balance_baxter_extra = sum_baxter_extra;
        console.log("balance vaso es: " + balance_baxter_extra);

        document.getElementById("input_baxter_extraplan").innerHTML = balance_baxter_extra;

        var multiplier = balance_baxter_extra;

        var baxter_extraplan_to_ml = 500;

        var baxter_extraplan_ml = +baxter_extraplan_to_ml * +multiplier;


        control_balance_AR1[2] = baxter_extraplan_ml;
        console.log(control_balance_AR1[2]);

    });

    document.getElementById("but_quitarbaxter_extraplan").addEventListener("click", function () {

        if (balance_baxter_extra === 0) {
            console.log("nada");
        } else { 

        var sum_baxter_extra = +balance_baxter_extra - 1;
        balance_baxter_extra = sum_baxter_extra;
        console.log("balance vaso es: " + balance_baxter_extra);

        document.getElementById("input_baxter_extraplan").innerHTML = balance_baxter_extra;

        var multiplier = balance_baxter_extra;

        var baxter_extraplan_to_ml = 500;

        var baxter_extraplan_ml = +baxter_extraplan_to_ml * +multiplier;


        control_balance_AR1[2] = baxter_extraplan_ml;
        console.log(control_balance_AR1[2]);
        }
    });

    document.getElementById("but_diuresis").addEventListener("click", function () {


        control_balance_AR1[3] = document.getElementById("input_diuresis").value;

        console.log(document.getElementById("input_diuresis").value);

        console.log(control_balance_AR1[3]);


    });

    document.getElementById("but_agregardeposicion").addEventListener("click", function () {

        var multiplier = document.getElementById("input_caca").value;

        var caca_to_ml = 200;

        var caca_ml = +caca_to_ml * +multiplier;


        control_balance_AR1[4] = caca_ml;
        console.log(control_balance_AR1[4]);
    });

    document.getElementById("but_agregarpeso").addEventListener("click", function () {

        control_balance_AR1[5] = document.getElementById("input_pesopaciente").value;
        console.log(control_balance_AR1[5]);

        //finalizar balance

        var ingresos_parcial = +control_balance_AR1[0] + +control_balance_AR1[1] + +control_balance_AR1[2];
        var egresos_parcial = +control_balance_AR1[3] + +control_balance_AR1[4];

        var agua_endogena = +control_balance_AR1[5] * 5;

        var perdidas_insensibles1 = control_balance_AR1[5] / 2

        var perdidas_insensibles2 = perdidas_insensibles1 * 24;

        var ingreso_total = +ingresos_parcial + +agua_endogena;

        var egreso_total = +egresos_parcial + +perdidas_insensibles2;

        var balance = +ingreso_total - +egreso_total;

        console.log(balance);

        document.getElementById("bal_total").innerHTML = balance;

        control_balance_AR1[6] = balance;

        //save balance array
        if (viewpac1_butpressed === true) {

            var save_balance_AR1 = control_balance_AR1;
            var save_balance_AR12 = JSON.stringify(save_balance_AR1);
            localStorage.setItem("save_balance_AR1_p1", save_balance_AR12);

        } else if (viewpac2_butpressed === true) {

            var save_balance_AR1 = control_balance_AR1;
            var save_balance_AR12 = JSON.stringify(save_balance_AR1);
            localStorage.setItem("save_balance_AR1_p2", save_balance_AR12);

        } else if (viewpac3_butpressed === true) {

            var save_balance_AR1 = control_balance_AR1;
            var save_balance_AR12 = JSON.stringify(save_balance_AR1);
            localStorage.setItem("save_balance_AR1_p3", save_balance_AR12);

        } else if (viewpac4_butpressed === true) {

            var save_balance_AR1 = control_balance_AR1;
            var save_balance_AR12 = JSON.stringify(save_balance_AR1);
            localStorage.setItem("save_balance_AR1_p4", save_balance_AR12);

        } else if (viewpac5_butpressed === true) {

            var save_balance_AR1 = control_balance_AR1;
            var save_balance_AR12 = JSON.stringify(save_balance_AR1);
            localStorage.setItem("save_balance_AR1_p5", save_balance_AR12);

        } else if (viewpac6_butpressed === true) {

            var save_balance_AR1 = control_balance_AR1;
            var save_balance_AR12 = JSON.stringify(save_balance_AR1);
            localStorage.setItem("save_balance_AR1_p6", save_balance_AR12);

        } else if (viewpac7_butpressed === true) {

            var save_balance_AR1 = control_balance_AR1;
            var save_balance_AR12 = JSON.stringify(save_balance_AR1);
            localStorage.setItem("save_balance_AR1_p7", save_balance_AR12);

        } else if (viewpac8_butpressed === true) {

            var save_balance_AR1 = control_balance_AR1;
            var save_balance_AR12 = JSON.stringify(save_balance_AR1);
            localStorage.setItem("save_balance_AR1_p8", save_balance_AR12);

        } else if (viewpac9_butpressed === true) {

            var save_balance_AR1 = control_balance_AR1;
            var save_balance_AR12 = JSON.stringify(save_balance_AR1);
            localStorage.setItem("save_balance_AR1_p9", save_balance_AR12);

        } else if (viewpac10_butpressed === true) {

            var save_balance_AR1 = control_balance_AR1;
            var save_balance_AR12 = JSON.stringify(save_balance_AR1);
            localStorage.setItem("save_balance_AR1_p10", save_balance_AR12);

        }

    });

}




//monitors tas buts

//monitor tas but 1 press 
function mon_tasbut1() {
    var tasbut = document.getElementById("div_TAS1");

    if (tasbut) {
        document.getElementById("addtas_bu1").onclick = function () {
            TASVALUE = prompt("Ingresar Tension Arterial Sistolica");

            if (viewpac1_butpressed === true) {
                control_hemodinamico_AR1[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1 1 which is: " + control_hemodinamico_AR1[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;

            } else if (viewpac2_butpressed === true) {
                control_hemodinamico_AR1B[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1B 1 which is: " + control_hemodinamico_AR1B[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;

            } else if (viewpac3_butpressed === true) {
                control_hemodinamico_AR1C[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1C 1 which is: " + control_hemodinamico_AR1C[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;

            } else if (viewpac4_butpressed === true) {
                control_hemodinamico_AR1D[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1D 1 which is: " + control_hemodinamico_AR1D[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;

            } else if (viewpac5_butpressed === true) {
                control_hemodinamico_AR1E[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1E 1 which is: " + control_hemodinamico_AR1E[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;

            } else if (viewpac6_butpressed === true) {
                control_hemodinamico_AR1F[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1F 1 which is: " + control_hemodinamico_AR1F[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;

            } else if (viewpac7_butpressed === true) {
                control_hemodinamico_AR1G[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1G 1 which is: " + control_hemodinamico_AR1G[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;

            } else if (viewpac8_butpressed === true) {
                control_hemodinamico_AR1H[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1H 1 which is: " + control_hemodinamico_AR1H[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;

            } else if (viewpac9_butpressed === true) {
                control_hemodinamico_AR1I[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1I 1 which is: " + control_hemodinamico_AR1I[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;

            } else if (viewpac10_butpressed === true) {
                control_hemodinamico_AR1J[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR1J 1 which is: " + control_hemodinamico_AR1J[1]);
                document.getElementById("TASval1").innerHTML = TASVALUE;
            }

            calc_TAM1();

          

                has_cont_hemodinamico_values = true;
                var save_hasvalue = has_cont_hemodinamico_values;
                var save_hasvalueb = JSON.stringify(save_hasvalue);
                localStorage.setItem("strd_cont_hem_hasvalue", save_hasvalueb);
            



            console.log("TAM to AR " + control_hemodinamico_AR1[3]);

            save_cont_hemTAS1();

        }
    }
}

function mon_tasbut2() {
    var tasbut = document.getElementById("div_TAS2");

    if (tasbut) {
        document.getElementById("addtas_bu2").onclick = function () {

            TASVALUE = prompt("Ingresar Tension Arterial Sistolica");

            if (viewpac1_butpressed === true){
              control_hemodinamico_AR2[1] = TASVALUE;
              console.log(TASVALUE + " is saved to AR2 1 which is: " + control_hemodinamico_AR2[1]);
              document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac2_butpressed === true) {
                control_hemodinamico_AR2B[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2B 1 which is: " + control_hemodinamico_AR2B[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac3_butpressed === true) {
                control_hemodinamico_AR2C[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2C 1 which is: " + control_hemodinamico_AR2C[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac4_butpressed === true) {
                control_hemodinamico_AR2D[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2D 1 which is: " + control_hemodinamico_AR2D[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac5_butpressed === true) {
                control_hemodinamico_AR2E[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2E 1 which is: " + control_hemodinamico_AR2E[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac6_butpressed === true) {
                control_hemodinamico_AR2F[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2F 1 which is: " + control_hemodinamico_AR2F[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac7_butpressed === true) {
                control_hemodinamico_AR2G[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2G 1 which is: " + control_hemodinamico_AR2G[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac8_butpressed === true) {
                control_hemodinamico_AR2H[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2H 1 which is: " + control_hemodinamico_AR2H[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac9_butpressed === true) {
                control_hemodinamico_AR2I[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2I 1 which is: " + control_hemodinamico_AR2I[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac10_butpressed === true) {
                control_hemodinamico_AR2J[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2J 1 which is: " + control_hemodinamico_AR2J[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            }
            calc_TAM2();

            has_cont_hemodinamico_values = true;
            var save_hasvalue = has_cont_hemodinamico_values;
            var save_hasvalueb = JSON.stringify(save_hasvalue);
            localStorage.setItem("strd_cont_hem_hasvalue", save_hasvalueb);



            console.log("TAM to AR " + control_hemodinamico_AR2[3]);

            save_cont_hemTAS2();

        }
    }
}

function mon_tasbut3() {
    var tasbut = document.getElementById("div_TAS3");

    if (tasbut) {
        document.getElementById("addtas_bu3").onclick = function () {
            TASVALUE = prompt("Ingresar Tension Arterial Sistolica");

            if (viewpac1_butpressed === true) {
                control_hemodinamico_AR3[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR3 1 which is: " + control_hemodinamico_AR3[1]);
                document.getElementById("TASval3").innerHTML = TASVALUE;
            } else if (viewpac2_butpressed === true) {
                control_hemodinamico_AR2B[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2B 1 which is: " + control_hemodinamico_AR2B[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac3_butpressed === true) {
                control_hemodinamico_AR3C[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2C 1 which is: " + control_hemodinamico_AR2C[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac4_butpressed === true) {
                control_hemodinamico_AR2D[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2D 1 which is: " + control_hemodinamico_AR2D[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac5_butpressed === true) {
                control_hemodinamico_AR2E[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2E 1 which is: " + control_hemodinamico_AR2E[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac6_butpressed === true) {
                control_hemodinamico_AR2F[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2F 1 which is: " + control_hemodinamico_AR2F[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac7_butpressed === true) {
                control_hemodinamico_AR2G[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2G 1 which is: " + control_hemodinamico_AR2G[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac8_butpressed === true) {
                control_hemodinamico_AR2H[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2H 1 which is: " + control_hemodinamico_AR2H[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac9_butpressed === true) {
                control_hemodinamico_AR2I[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2I 1 which is: " + control_hemodinamico_AR2I[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            } else if (viewpac10_butpressed === true) {
                control_hemodinamico_AR2J[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR2J 1 which is: " + control_hemodinamico_AR2J[1]);
                document.getElementById("TASval2").innerHTML = TASVALUE;

            }

            calc_TAM3();

            has_cont_hemodinamico_values = true;
            var save_hasvalue = has_cont_hemodinamico_values;
            var save_hasvalueb = JSON.stringify(save_hasvalue);
            localStorage.setItem("strd_cont_hem_hasvalue", save_hasvalueb);



            console.log("TAM to AR " + control_hemodinamico_AR3[3]);

            save_cont_hemTAS3();

        }
    }
}

function mon_tasbut4() {
    var tasbut = document.getElementById("div_TAS4");

    if (tasbut) {
        document.getElementById("addtas_bu4").onclick = function () {
            TASVALUE = prompt("Ingresar Tension Arterial Sistolica");

            if (viewpac1_butpressed === true){
            control_hemodinamico_AR4[1] = TASVALUE;
            console.log(TASVALUE + " is saved to AR4 1 which is: " + control_hemodinamico_AR4[1]);
            document.getElementById("TASval4").innerHTML = TASVALUE;

            } else if (viewpac2_butpressed === true) {
                control_hemodinamico_AR4B[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR4B 1 which is: " + control_hemodinamico_AR4B[1]);
                document.getElementById("TASval4").innerHTML = TASVALUE;

            } else if (viewpac3_butpressed === true) {
                control_hemodinamico_AR4C[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR4C 1 which is: " + control_hemodinamico_AR4C[1]);
                document.getElementById("TASval4").innerHTML = TASVALUE;

            } else if (viewpac4_butpressed === true) {
                control_hemodinamico_AR4D[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR4D 1 which is: " + control_hemodinamico_AR4D[1]);
                document.getElementById("TASval4").innerHTML = TASVALUE;

            } else if (viewpac5_butpressed === true) {
                control_hemodinamico_AR4E[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR4E 1 which is: " + control_hemodinamico_AR4E[1]);
                document.getElementById("TASval4").innerHTML = TASVALUE;

            } else if (viewpac6_butpressed === true) {
                control_hemodinamico_AR4F[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR4F 1 which is: " + control_hemodinamico_AR4F[1]);
                document.getElementById("TASval4").innerHTML = TASVALUE;

            } else if (viewpac7_butpressed === true) {
                control_hemodinamico_AR4G[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR4G 1 which is: " + control_hemodinamico_AR4G[1]);
                document.getElementById("TASval4").innerHTML = TASVALUE;

            } else if (viewpac8_butpressed === true) {
                control_hemodinamico_AR4H[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR4H 1 which is: " + control_hemodinamico_AR4H[1]);
                document.getElementById("TASval4").innerHTML = TASVALUE;

            } else if (viewpac9_butpressed === true) {
                control_hemodinamico_AR4I[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR4I 1 which is: " + control_hemodinamico_AR4I[1]);
                document.getElementById("TASval4").innerHTML = TASVALUE;

            } else if (viewpac10_butpressed === true) {
                control_hemodinamico_AR4J[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR4J 1 which is: " + control_hemodinamico_AR4J[1]);
                document.getElementById("TASval4").innerHTML = TASVALUE;

            }

            calc_TAM4();

            has_cont_hemodinamico_values = true;
            var save_hasvalue = has_cont_hemodinamico_values;
            var save_hasvalueb = JSON.stringify(save_hasvalue);
            localStorage.setItem("strd_cont_hem_hasvalue", save_hasvalueb);



            console.log("TAM to AR " + control_hemodinamico_AR4[3]);

            save_cont_hemTAS4();

        }
    }
}

function mon_tasbut5() {
    var tasbut = document.getElementById("div_TAS5");

    if (tasbut) {
        document.getElementById("addtas_bu5").onclick = function () {
            TASVALUE = prompt("Ingresar Tension Arterial Sistolica");


            if (viewpac1_butpressed === true){
            control_hemodinamico_AR5[1] = TASVALUE;
            console.log(TASVALUE + " is saved to AR5 1 which is: " + control_hemodinamico_AR5[1]);
            document.getElementById("TASval5").innerHTML = TASVALUE;

            } else if (viewpac2_butpressed === true) {
                control_hemodinamico_AR5B[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR5B 1 which is: " + control_hemodinamico_AR5B[1]);
                document.getElementById("TASval5").innerHTML = TASVALUE;

            } else if (viewpac3_butpressed === true) {
                control_hemodinamico_AR5C[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR5C 1 which is: " + control_hemodinamico_AR5C[1]);
                document.getElementById("TASval5").innerHTML = TASVALUE;

            } else if (viewpac4_butpressed === true) {
                control_hemodinamico_AR5D[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR5D 1 which is: " + control_hemodinamico_AR5D[1]);
                document.getElementById("TASval5").innerHTML = TASVALUE;

            } else if (viewpac5_butpressed === true) {
                control_hemodinamico_AR5E[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR5E 1 which is: " + control_hemodinamico_AR5E[1]);
                document.getElementById("TASval5").innerHTML = TASVALUE;

            } else if (viewpac6_butpressed === true) {
                control_hemodinamico_AR5F[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR5F 1 which is: " + control_hemodinamico_AR5F[1]);
                document.getElementById("TASval5").innerHTML = TASVALUE;

            } else if (viewpac7_butpressed === true) {
                control_hemodinamico_AR5G[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR5G 1 which is: " + control_hemodinamico_AR5G[1]);
                document.getElementById("TASval5").innerHTML = TASVALUE;

            } else if (viewpac8_butpressed === true) {
                control_hemodinamico_AR5H[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR5H 1 which is: " + control_hemodinamico_AR5H[1]);
                document.getElementById("TASval5").innerHTML = TASVALUE;

            } else if (viewpac9_butpressed === true) {
                control_hemodinamico_AR5I[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR5I 1 which is: " + control_hemodinamico_AR5I[1]);
                document.getElementById("TASval5").innerHTML = TASVALUE;

            } else if (viewpac10_butpressed) {
                control_hemodinamico_AR5J[1] = TASVALUE;
                console.log(TASVALUE + " is saved to AR5J 1 which is: " + control_hemodinamico_AR5J[1]);
                document.getElementById("TASval5").innerHTML = TASVALUE;

            }

            calc_TAM5();

            has_cont_hemodinamico_values = true;
            var save_hasvalue = has_cont_hemodinamico_values;
            var save_hasvalueb = JSON.stringify(save_hasvalue);
            localStorage.setItem("strd_cont_hem_hasvalue", save_hasvalueb);



            console.log("TAM to AR " + control_hemodinamico_AR5[3]);

            save_cont_hemTAS5();

        }
    }
}


//monitor tad buttons
//monitor tad but 1 press
function mon_tadbut1() {
    var tadbut = document.getElementById("div_TAD1");

    if (tadbut) {
        document.getElementById("addtad_bu1").onclick = function () {
            TADVALUE = prompt("Ingresar Tension Arterial Diastolica");

            if (viewpac1_butpressed === true) {
                control_hemodinamico_AR1[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            } else if (viewpac2_butpressed === true) {
                control_hemodinamico_AR1B[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            } else if (viewpac3_butpressed === true) {
                control_hemodinamico_AR1C[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            } else if (viewpac4_butpressed === true) {
                control_hemodinamico_AR1D[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            } else if (viewpac5_butpressed === true) {
                control_hemodinamico_AR1E[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            } else if (viewpac6_butpressed === true) {
                control_hemodinamico_AR1F[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            } else if (viewpac7_butpressed === true) {
                control_hemodinamico_AR1G[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            } else if (viewpac8_butpressed === true) {
                control_hemodinamico_AR1H[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            } else if (viewpac9_butpressed === true) {
                control_hemodinamico_AR1I[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            } else if (viewpac10_butpressed === true) {
                control_hemodinamico_AR1J[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval1").innerHTML = TADVALUE;

            }

            calc_TAM1();



            console.log("TAM to AR " + control_hemodinamico_AR1[3]);

            save_cont_hemTAD1()
        }
    }
}

    function mon_tadbut2() {
        var tadbut = document.getElementById("div_TAD2");

        if (tadbut) {
            document.getElementById("addtad_bu2").onclick = function () {
                TADVALUE = prompt("Ingresar Tension Arterial Diastolica");

                if (viewpac1_butpressed === true){
                control_hemodinamico_AR2[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval2").innerHTML = TADVALUE;

                } else if (viewpac2_butpressed === true) {
                    control_hemodinamico_AR2B[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval2").innerHTML = TADVALUE;

                } else if (viewpac3_butpressed === true) {
                    control_hemodinamico_AR2C[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval2").innerHTML = TADVALUE;

                } else if (viewpac4_butpressed === true) {
                    control_hemodinamico_AR2D[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval2").innerHTML = TADVALUE;

                } else if (viewpac5_butpressed === true) {
                    control_hemodinamico_AR2E[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval2").innerHTML = TADVALUE;

                } else if (viewpac6_butpressed === true) {
                    control_hemodinamico_AR2F[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval2").innerHTML = TADVALUE;

                } else if (viewpac7_butpressed === true) {
                    control_hemodinamico_AR2G[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval2").innerHTML = TADVALUE;

                } else if (viewpac8_butpressed === true) {
                    control_hemodinamico_AR2H[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval2").innerHTML = TADVALUE;

                } else if (viewpac9_butpressed === true) {
                    control_hemodinamico_AR2I[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval2").innerHTML = TADVALUE;

                } else if (viewpac10_butpressed === true){
                    control_hemodinamico_AR2J[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval2").innerHTML = TADVALUE;

                }
                

                calc_TAM2();



                console.log("TAM to AR " + control_hemodinamico_AR2[3]);

                save_cont_hemTAD2()
            }
        }
}

    function mon_tadbut3() {
        var tadbut = document.getElementById("div_TAD3");

        if (tadbut) {
            document.getElementById("addtad_bu3").onclick = function () {
                TADVALUE = prompt("Ingresar Tension Arterial Diastolica");

                if (viewpac1_butpressed === true){
                control_hemodinamico_AR3[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval3").innerHTML = TADVALUE;

                } else if (viewpac2_butpressed === true) {
                    control_hemodinamico_AR3B[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval3").innerHTML = TADVALUE;

                } else if (viewpac3_butpressed === true) {
                    control_hemodinamico_AR3C[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval3").innerHTML = TADVALUE;

                } else if (viewpac4_butpressed === true) {
                    control_hemodinamico_AR3D[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval3").innerHTML = TADVALUE;

                } else if (viewpac5_butpressed === true) {
                    control_hemodinamico_AR3E[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval3").innerHTML = TADVALUE;

                } else if (viewpac6_butpressed === true) {
                    control_hemodinamico_AR3F[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval3").innerHTML = TADVALUE;

                } else if (viewpac7_butpressed === true) {
                    control_hemodinamico_AR3G[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval3").innerHTML = TADVALUE;

                } else if (viewpac8_butpressed === true) {
                    control_hemodinamico_AR3H[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval3").innerHTML = TADVALUE;

                } else if (viewpac9_butpressed === true) {
                    control_hemodinamico_AR3I[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval3").innerHTML = TADVALUE;

                } else if (viewpac10_butpressed === true) {
                    control_hemodinamico_AR3J[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval3").innerHTML = TADVALUE;

                }

                calc_TAM3();

                console.log("TAM to AR " + control_hemodinamico_AR3[3]);

                save_cont_hemTAD3()
            }
        }
    }

    function mon_tadbut4() {
        var tadbut = document.getElementById("div_TAD4");

        if (tadbut) {
            document.getElementById("addtad_bu4").onclick = function () {
                TADVALUE = prompt("Ingresar Tension Arterial Diastolica");

                if (viewpac1_butpressed === true){
                control_hemodinamico_AR4[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval4").innerHTML = TADVALUE;

                } else if (viewpac2_butpressed === true) {
                    control_hemodinamico_AR4B[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval4").innerHTML = TADVALUE;

                } else if (viewpac3_butpressed === true) {
                    control_hemodinamico_AR4C[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval4").innerHTML = TADVALUE;

                } else if (viewpac4_butpressed === true) {
                    control_hemodinamico_AR4D[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval4").innerHTML = TADVALUE;

                } else if (viewpac5_butpressed === true) {
                    control_hemodinamico_AR4E[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval4").innerHTML = TADVALUE;

                } else if (viewpac6_butpressed === true) {
                    control_hemodinamico_AR4F[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval4").innerHTML = TADVALUE;

                } else if (viewpac7_butpressed === true) {
                    control_hemodinamico_AR4G[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval4").innerHTML = TADVALUE;

                } else if (viewpac8_butpressed === true) {
                    control_hemodinamico_AR4H[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval4").innerHTML = TADVALUE;

                } else if (viewpac9_butpressed === true) {
                    control_hemodinamico_AR4I[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval4").innerHTML = TADVALUE;

                } else if (viewpac10_butpressed === true) {
                    control_hemodinamico_AR4J[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval4").innerHTML = TADVALUE;

                }
                
             

                calc_TAM4();



                console.log("TAM to AR " + control_hemodinamico_AR4[3]);

                save_cont_hemTAD4()
            }
        }
    }


    function mon_tadbut5() {
        var tadbut = document.getElementById("div_TAD5");

        if (tadbut) {
            document.getElementById("addtad_bu5").onclick = function () {
                TADVALUE = prompt("Ingresar Tension Arterial Diastolica");

                if (viewpac1_butpressed === true){
                control_hemodinamico_AR5[2] = TADVALUE;
                console.log(TADVALUE);
                document.getElementById("TADval5").innerHTML = TADVALUE;

                } else if (viewpac2_butpressed === true) {
                    control_hemodinamico_AR5B[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval5").innerHTML = TADVALUE;

                } else if (viewpac3_butpressed === true) {
                    control_hemodinamico_AR5C[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval5").innerHTML = TADVALUE;

                } else if (viewpac4_butpressed === true) {
                    control_hemodinamico_AR5D[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval5").innerHTML = TADVALUE;

                } else if (viewpac5_butpressed === true) {
                    control_hemodinamico_AR5E[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval5").innerHTML = TADVALUE;

                } else if (viewpac6_butpressed === true) {
                    control_hemodinamico_AR5F[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval5").innerHTML = TADVALUE;

                } else if (viewpac7_butpressed === true) {
                    control_hemodinamico_AR5G[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval5").innerHTML = TADVALUE;

                } else if (viewpac8_butpressed === true) {
                    control_hemodinamico_AR5H[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval5").innerHTML = TADVALUE;

                } else if (viewpac9_butpressed === true) {
                    control_hemodinamico_AR5I[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval5").innerHTML = TADVALUE;

                } else if (viewpac10_butpressed === true) {
                    control_hemodinamico_AR5J[2] = TADVALUE;
                    console.log(TADVALUE);
                    document.getElementById("TADval5").innerHTML = TADVALUE;

                }

                calc_TAM5();



                console.log("TAM to AR " + control_hemodinamico_AR5[3]);

                save_cont_hemTAD5()
            }
        }
    }



    //monitor fc buttons
    //monitor fc1 button
    function mon_fcbut1() {
        var fcbut = document.getElementById("addfc_bu1");

        if (fcbut) {
            document.getElementById("addfc_bu1").onclick = function () {
                FCVALUE = prompt("Ingresar Frecuencia Cardiaca");

                if (viewpac1_butpressed === true){
                control_hemodinamico_AR1[3] = FCVALUE;
                console.log(FCVALUE);
                document.getElementById("fcval1").innerHTML = FCVALUE;

                } else if (viewpac2_butpressed === true) {
                    control_hemodinamico_AR1B[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval1").innerHTML = FCVALUE;

                } else if (viewpac3_butpressed === true) {
                    control_hemodinamico_AR1C[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval1").innerHTML = FCVALUE;

                } else if (viewpac4_butpressed === true) {
                    control_hemodinamico_AR1D[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval1").innerHTML = FCVALUE;

                } else if (viewpac5_butpressed === true) {
                    control_hemodinamico_AR1E[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval1").innerHTML = FCVALUE;

                } else if (viewpac6_butpressed === true) {
                    control_hemodinamico_AR1F[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval1").innerHTML = FCVALUE;

                } else if (viewpac7_butpressed === true) {
                    control_hemodinamico_AR1G[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval1").innerHTML = FCVALUE;

                } else if (viewpac8_butpressed === true) {
                    control_hemodinamico_AR1H[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval1").innerHTML = FCVALUE;

                } else if (viewpac9_butpressed === true) {
                    control_hemodinamico_AR1I[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval1").innerHTML = FCVALUE;

                } else if (viewpac10_butpressed === true) {
                    control_hemodinamico_AR1J[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval1").innerHTML = FCVALUE;

                }

                control_hemodinamico_AR1[3] = FCVALUE;

                save_cont_hemFC1();

            }
        }
    }

    function mon_fcbut2() {
        var fcbut = document.getElementById("addfc_bu2");

        if (fcbut) {
            document.getElementById("addfc_bu2").onclick = function () {
                FCVALUE = prompt("Ingresar Frecuencia Cardiaca");

                if (viewpac1_butpressed === true) {
                    control_hemodinamico_AR2[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval2").innerHTML = FCVALUE;

                } else if (viewpac2_butpressed === true) {
                    control_hemodinamico_AR2B[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval2").innerHTML = FCVALUE;

                } else if (viewpac3_butpressed === true) {
                    control_hemodinamico_AR2C[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval2").innerHTML = FCVALUE;

                } else if (viewpac4_butpressed === true) {
                    control_hemodinamico_AR2D[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval2").innerHTML = FCVALUE;

                } else if (viewpac5_butpressed === true) {
                    control_hemodinamico_AR2E[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval2").innerHTML = FCVALUE;

                } else if (viewpac6_butpressed === true) {
                    control_hemodinamico_AR2F[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval2").innerHTML = FCVALUE;

                }else if (viewpac7_butpressed === true) {
                     control_hemodinamico_AR2G[3] = FCVALUE;
                     console.log(FCVALUE);
                     document.getElementById("fcval2").innerHTML = FCVALUE;

                } else if (viewpac8_butpressed === true) {
                    control_hemodinamico_AR2H[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval2").innerHTML = FCVALUE;

                } else if (viewpac9_butpressed === true) {
                    control_hemodinamico_AR2I[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval2").innerHTML = FCVALUE;

                } else if (viewpac10_butpressed === true) {
                    control_hemodinamico_AR2J[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval2").innerHTML = FCVALUE;

                }


                //control_hemodinamico_AR2[3] = FCVALUE;

                save_cont_hemFC2();

            }
        }
    }

    function mon_fcbut3() {
        var fcbut = document.getElementById("addfc_bu3");

        if (fcbut) {
            document.getElementById("addfc_bu3").onclick = function () {
                FCVALUE = prompt("Ingresar Frecuencia Cardiaca");

                if (viewpac1_butpressed === true){
                control_hemodinamico_AR3[3] = FCVALUE;
                console.log(FCVALUE);
                document.getElementById("fcval3").innerHTML = FCVALUE;

                } else if (viewpac2_butpressed === true) {
                    control_hemodinamico_AR3B[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval3").innerHTML = FCVALUE;

                } else if (viewpac3_butpressed === true) {
                    control_hemodinamico_AR3C[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval3").innerHTML = FCVALUE;

                } else if (viewpac4_butpressed === true) {
                    control_hemodinamico_AR3D[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval3").innerHTML = FCVALUE;

                } else if (viewpac5_butpressed === true) {
                    control_hemodinamico_AR3E[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval3").innerHTML = FCVALUE;

                } else if (viewpac6_butpressed === true) {
                    control_hemodinamico_AR3F[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval3").innerHTML = FCVALUE;

                } else if (viewpac7_butpressed === true) {
                    control_hemodinamico_AR3G[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval3").innerHTML = FCVALUE;

                } else if (viewpac8_butpressed === true) {
                    control_hemodinamico_AR3H[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval3").innerHTML = FCVALUE;

                } else if (viewpac9_butpressed === true) {
                    control_hemodinamico_AR3I[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval3").innerHTML = FCVALUE;

                } else if (viewpac10_butpressed === true) {
                    control_hemodinamico_AR3J[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval3").innerHTML = FCVALUE;

                }
                
                //control_hemodinamico_AR3[3] = FCVALUE;

                save_cont_hemFC3();

            }
        }
    }

    function mon_fcbut4() {
        var fcbut = document.getElementById("addfc_bu4");

        if (fcbut) {
            document.getElementById("addfc_bu4").onclick = function () {
                FCVALUE = prompt("Ingresar Frecuencia Cardiaca");
                control_hemodinamico_AR4[3] = FCVALUE;
                console.log(FCVALUE);
                document.getElementById("fcval4").innerHTML = FCVALUE;

               // control_hemodinamico_AR4[3] = FCVALUE;

                save_cont_hemFC4();

            }
        }
    }

    function mon_fcbut5() {
        var fcbut = document.getElementById("addfc_bu5");

        if (fcbut) {
            document.getElementById("addfc_bu5").onclick = function () {
                FCVALUE = prompt("Ingresar Frecuencia Cardiaca");

                if (viewpac1_butpressed === true){
                control_hemodinamico_AR5[3] = FCVALUE;
                console.log(FCVALUE);
                document.getElementById("fcval5").innerHTML = FCVALUE;

                } else if (viewpac2_butpressed === true) {
                    control_hemodinamico_AR5B[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval5").innerHTML = FCVALUE;

                } else if (viewpac3_butpressed === true) {
                    control_hemodinamico_AR5C[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval5").innerHTML = FCVALUE;

                } else if (viewpac4_butpressed === true) {
                    control_hemodinamico_AR5D[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval5").innerHTML = FCVALUE;

                } else if (viewpac5_butpressed === true) {
                    control_hemodinamico_AR5E[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval5").innerHTML = FCVALUE;

                } else if (viewpac6_butpressed === true) {
                    control_hemodinamico_AR5F[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval5").innerHTML = FCVALUE;

                } else if (viewpac7_butpressed === true) {
                    control_hemodinamico_AR5G[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval5").innerHTML = FCVALUE;

                } else if (viewpac8_butpressed === true) {
                    control_hemodinamico_AR5H[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval5").innerHTML = FCVALUE;

                } else if (viewpac9_butpressed === true) {
                    control_hemodinamico_AR5I[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval5").innerHTML = FCVALUE;

                } else if (viewpac10_butpressed === true) {
                    control_hemodinamico_AR5J[3] = FCVALUE;
                    console.log(FCVALUE);
                    document.getElementById("fcval5").innerHTML = FCVALUE;

                }

                //control_hemodinamico_AR5[3] = FCVALUE;

                save_cont_hemFC5();

            }
        }
    }

    //CALCULAR TAM
    function calc_TAM1() {
        var c_tas = TASVALUE;
        var c_tad = TADVALUE;

        var c1 = c_tad * 2;
        console.log(c1);
        var c2 = +c1 + +c_tas;
        console.log(c2);
        var c3 = c2 / 3;
        console.log(c3);
        TAM1 = c3;
        console.log(TAM1);

        if (viewpac1_butpressed === true) {
            control_hemodinamico_AR1[4] = TAM1;
            document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1[4];

        } else if (viewpac2_butpressed === true) {
            control_hemodinamico_AR1B[4] = TAM1;
            document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1B[4];

        } else if (viewpac3_butpressed === true) {
            control_hemodinamico_AR1C[4] = TAM1;
            document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1C[4];

        }else if (viewpac4_butpressed === true) {
             control_hemodinamico_AR1D[4] = TAM1;
             document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1D[4];

        } else if (viewpac5_butpressed === true) {
            control_hemodinamico_AR1E[4] = TAM1;
            document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1E[4];

        } else if (viewpac6_butpressed === true) {
            control_hemodinamico_AR1F[4] = TAM1;
            document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1F[4];

        } else if (viewpac7_butpressed === true) {
            control_hemodinamico_AR1G[4] = TAM1;
            document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1G[4];

        } else if (viewpac8_butpressed === true) {
            control_hemodinamico_AR1H[4] = TAM1;
            document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1H[4];

        } else if (viewpac9_butpressed === true) {
            control_hemodinamico_AR1I[4] = TAM1;
            document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1I[4];

        } else if (viewpac10_butpressed === true) {
            control_hemodinamico_AR1J[4] = TAM1;
            document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1J[4];

        }
      

        save_cont_hemTAM1();




    }

    function calc_TAM2() {
        var c_tas = TASVALUE;
        var c_tad = TADVALUE;

        var c1 = c_tad * 2;
        console.log(c1);
        var c2 = +c1 + +c_tas;
        console.log(c2);
        var c3 = c2 / 3;
        console.log(c3);
        TAM2 = c3;
        console.log(TAM2);


        if (viewpac1_butpressed === true) {
            control_hemodinamico_AR2[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2[4];

        } else if (viewpac2_butpressed === true) {
            control_hemodinamico_AR2B[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2B[4];

        } else if (viewpac3_butpressed === true) {
            control_hemodinamico_AR2C[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2C[4];

        } else if (viewpac4_butpressed === true) {
            control_hemodinamico_AR2D[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2D[4];

        } else if (viewpac5_butpressed === true) {
            control_hemodinamico_AR2E[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2E[4];

        } else if (viewpac6_butpressed === true) {
            control_hemodinamico_AR2F[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2F[4];

        } else if (viewpac7_butpressed === true) {
            control_hemodinamico_AR2G[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2G[4];

        } else if (viewpac8_butpressed === true) {
            control_hemodinamico_AR2H[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2H[4];

        } else if (viewpac9_butpressed === true) {
            control_hemodinamico_AR2I[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2I[4];

        } else if (viewpac10_butpressed === true) {
            control_hemodinamico_AR2J[4] = TAM2;
            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2J[4];

        }

        save_cont_hemTAM2();

    }

    function calc_TAM3() {
        var c_tas = TASVALUE;
        var c_tad = TADVALUE;

        var c1 = c_tad * 2;
        console.log(c1);
        var c2 = +c1 + +c_tas;
        console.log(c2);
        var c3 = c2 / 3;
        console.log(c3);
        TAM3 = c3;
        console.log(TAM3);

        if (viewpac1_butpressed === true) {
            control_hemodinamico_AR3[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3[4];

        } else if (viewpac2_butpressed === true) {
            control_hemodinamico_AR3B[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3B[4];

        } else if (viewpac3_butpressed === true) {
            control_hemodinamico_AR3C[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3C[4];

        } else if (viewpac4_butpressed === true) {
            control_hemodinamico_AR3D[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3D[4];

        } else if (viewpac5_butpressed === true) {
            control_hemodinamico_AR3E[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3E[4];

        } else if (viewpac6_butpressed === true) {
            control_hemodinamico_AR3F[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3F[4];

        } else if (viewpac7_butpressed === true) {
            control_hemodinamico_AR3G[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3G[4];

        } else if (viewpac8_butpressed === true) {
            control_hemodinamico_AR3H[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3H[4];

        } else if (viewpac9_butpressed === true) {
            control_hemodinamico_AR3I[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3I[4];

        } else if (viewpac10_butpressed === true) {
            control_hemodinamico_AR3J[4] = TAM3;
            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3J[4];

        }

        save_cont_hemTAM3();




    }

    function calc_TAM4() {
        var c_tas = TASVALUE;
        var c_tad = TADVALUE;

        var c1 = c_tad * 2;
        console.log(c1);
        var c2 = +c1 + +c_tas;
        console.log(c2);
        var c3 = c2 / 3;
        console.log(c3);
        TAM4 = c3;
        console.log(TAM4);

        if (viewpac1_butpressed === true){
        control_hemodinamico_AR4[4] = TAM4;
        document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4[4];

        } else if (viewpac2_butpressed === true) {
            control_hemodinamico_AR4B[4] = TAM4;
            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4B[4];

        } else if (viewpac3_butpressed === true) {
            control_hemodinamico_AR4C[4] = TAM4;
            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4C[4];

        } else if (viewpac4_butpressed === true) {
            control_hemodinamico_AR4D[4] = TAM4;
            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4D[4];

        } else if (viewpac5_butpressed === true) {
            control_hemodinamico_AR4E[4] = TAM4;
            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4E[4];

        } else if (viewpac6_butpressed === true) {
            control_hemodinamico_AR4F[4] = TAM4;
            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4F[4];

        } else if (viewpac7_butpressed === true) {
            control_hemodinamico_AR4G[4] = TAM4;
            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4G[4];

        } else if (viewpac8_butpressed === true) {
            control_hemodinamico_AR4H[4] = TAM4;
            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4H[4];

        } else if (viewpac9_butpressed === true) {
            control_hemodinamico_AR4I[4] = TAM4;
            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4I[4];

        } else if (viewpac10_butpressed === true) {
            control_hemodinamico_AR4J[4] = TAM4;
            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4J[4];

        }

        save_cont_hemTAM4();




    }


    function calc_TAM5() {
        var c_tas = TASVALUE;
        var c_tad = TADVALUE;

        var c1 = c_tad * 2;
        console.log(c1);
        var c2 = +c1 + +c_tas;
        console.log(c2);
        var c3 = c2 / 3;
        console.log(c3);
        TAM5 = c3;
        console.log(TAM5);

        if (viewpac1_butpressed === true) {
            control_hemodinamico_AR5[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5[4];

        } else if (viewpac2_butpressed === true) {
            control_hemodinamico_AR5B[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5B[4];

        } else if (viewpac3_butpressed === true) {
            control_hemodinamico_AR5C[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5C[4];

        } else if (viewpac4_butpressed === true) {
            control_hemodinamico_AR5D[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5D[4];

        } else if (viewpac5_butpressed === true) {
            control_hemodinamico_AR5E[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5E[4];

        } else if (viewpac6_butpressed === true) {
            control_hemodinamico_AR5F[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5F[4];

        } else if (viewpac7_butpressed === true) {
            control_hemodinamico_AR5G[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5G[4];

        } else if (viewpac8_butpressed === true) {
            control_hemodinamico_AR5H[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5H[4];

        } else if (viewpac9_butpressed === true) {
            control_hemodinamico_AR5I[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5I[4];

        } else if (viewpac10_butpressed === true) {
            control_hemodinamico_AR5J[4] = TAM5;
            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5J[4];

        }


        save_cont_hemTAM5();




    }



    //save control hemodinamico AR to local storage
    function save_cont_hemTAS1() {
        if (viewpac1_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1[1] + " SAVED");

        } else if (viewpac2_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1B[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1B", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1B[1] + " SAVED");

        } else if (viewpac3_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1C[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1C", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1C[1] + " SAVED");

        } else if (viewpac4_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1D[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1D", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1D[1] + " SAVED");

        } else if (viewpac5_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1E[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1E", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1E[1] + " SAVED");

        } else if (viewpac6_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1F[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1F", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1F[1] + " SAVED");

        } else if (viewpac7_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1G[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1G", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1G[1] + " SAVED");

        } else if (viewpac8_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1H[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1H", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1H[1] + " SAVED");

        } else if (viewpac9_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1I[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1I", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1I[1] + " SAVED");

        } else if (viewpac10_butpressed === true) {

            var save_conthemTAS1 = control_hemodinamico_AR1J[1];
            var save_conthemTAS1b = JSON.stringify(save_conthemTAS1);
            localStorage.setItem("saved_cont_hemTAS1J", save_conthemTAS1b);
            console.log(control_hemodinamico_AR1J[1] + " SAVED");

        }
    }

    function save_cont_hemTAD1() {

        if (viewpac1_butpressed === true){
        var save_conthemTAD1 = control_hemodinamico_AR1[2];
        var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
        localStorage.setItem("saved_cont_hemTAD1", save_conthemTAD1b);
        console.log(control_hemodinamico_AR1[2] + " SAVED");

        } else if (viewpac2_butpressed === true) {
            var save_conthemTAD1 = control_hemodinamico_AR1B[2];
            var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
            localStorage.setItem("saved_cont_hemTAD1B", save_conthemTAD1b);
            console.log(control_hemodinamico_AR1B[2] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemTAD1 = control_hemodinamico_AR1C[2];
            var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
            localStorage.setItem("saved_cont_hemTAD1C", save_conthemTAD1b);
            console.log(control_hemodinamico_AR1C[2] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemTAD1 = control_hemodinamico_AR1D[2];
            var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
            localStorage.setItem("saved_cont_hemTAD1D", save_conthemTAD1b);
            console.log(control_hemodinamico_AR1D[2] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemTAD1 = control_hemodinamico_AR1E[2];
            var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
            localStorage.setItem("saved_cont_hemTAD1E", save_conthemTAD1b);
            console.log(control_hemodinamico_AR1E[2] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemTAD1 = control_hemodinamico_AR1F[2];
            var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
            localStorage.setItem("saved_cont_hemTAD1F", save_conthemTAD1b);
            console.log(control_hemodinamico_AR1F[2] + " SAVED");

        } else if (viewpac7_butpressed === true) {
            var save_conthemTAD1 = control_hemodinamico_AR1G[2];
            var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
            localStorage.setItem("saved_cont_hemTAD1G", save_conthemTAD1b);
            console.log(control_hemodinamico_AR1G[2] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemTAD1 = control_hemodinamico_AR1H[2];
            var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
            localStorage.setItem("saved_cont_hemTAD1H", save_conthemTAD1b);
            console.log(control_hemodinamico_AR1H[2] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemTAD1 = control_hemodinamico_AR1I[2];
            var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
            localStorage.setItem("saved_cont_hemTAD1I", save_conthemTAD1b);
            console.log(control_hemodinamico_AR1I[2] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemTAD1 = control_hemodinamico_AR1J[2];
            var save_conthemTAD1b = JSON.stringify(save_conthemTAD1);
            localStorage.setItem("saved_cont_hemTAD1J", save_conthemTAD1b);
            console.log(control_hemodinamico_AR1J[2] + " SAVED");

        }
    }

    function save_cont_hemFC1() {

        if (viewpac1_butpressed === true){
        var save_conthemFC1 = control_hemodinamico_AR1[3];
        var save_conthemFC1b = JSON.stringify(save_conthemFC1);
        localStorage.setItem("saved_cont_hemFC1", save_conthemFC1b);
        console.log(control_hemodinamico_AR1[3] + " SAVED");

        } else if (viewpac2_butpressed === true) {
            var save_conthemFC1 = control_hemodinamico_AR1B[3];
            var save_conthemFC1b = JSON.stringify(save_conthemFC1);
            localStorage.setItem("saved_cont_hemFC1B", save_conthemFC1b);
            console.log(control_hemodinamico_AR1B[3] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemFC1 = control_hemodinamico_AR1C[3];
            var save_conthemFC1b = JSON.stringify(save_conthemFC1);
            localStorage.setItem("saved_cont_hemFC1C", save_conthemFC1b);
            console.log(control_hemodinamico_AR1C[3] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemFC1 = control_hemodinamico_AR1D[3];
            var save_conthemFC1b = JSON.stringify(save_conthemFC1);
            localStorage.setItem("saved_cont_hemFC1D", save_conthemFC1b);
            console.log(control_hemodinamico_AR1D[3] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemFC1 = control_hemodinamico_AR1E[3];
            var save_conthemFC1b = JSON.stringify(save_conthemFC1);
            localStorage.setItem("saved_cont_hemFC1E", save_conthemFC1b);
            console.log(control_hemodinamico_AR1E[3] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemFC1 = control_hemodinamico_AR1F[3];
            var save_conthemFC1b = JSON.stringify(save_conthemFC1);
            localStorage.setItem("saved_cont_hemFC1F", save_conthemFC1b);
            console.log(control_hemodinamico_AR1F[3] + " SAVED");

        } else if (viewpac7_butpressed === true) {
            var save_conthemFC1 = control_hemodinamico_AR1G[3];
            var save_conthemFC1b = JSON.stringify(save_conthemFC1);
            localStorage.setItem("saved_cont_hemFC1G", save_conthemFC1b);
            console.log(control_hemodinamico_AR1G[3] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemFC1 = control_hemodinamico_AR1H[3];
            var save_conthemFC1b = JSON.stringify(save_conthemFC1);
            localStorage.setItem("saved_cont_hemFC1H", save_conthemFC1b);
            console.log(control_hemodinamico_AR1H[3] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemFC1 = control_hemodinamico_AR1I[3];
            var save_conthemFC1b = JSON.stringify(save_conthemFC1);
            localStorage.setItem("saved_cont_hemFC1I", save_conthemFC1b);
            console.log(control_hemodinamico_AR1I[3] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemFC1 = control_hemodinamico_AR1J[3];
            var save_conthemFC1b = JSON.stringify(save_conthemFC1);
            localStorage.setItem("saved_cont_hemFC1J", save_conthemFC1b);
            console.log(control_hemodinamico_AR1J[3] + " SAVED");

        }

    }

    function save_cont_hemTAM1() {

        if (viewpac1_butpressed === true) {
            var save_conthemTAM1 = control_hemodinamico_AR1[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAM1", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1[4] + " SAVED");

        } else if (viewpac2_butpressed === true){
            var save_conthemTAM1 = control_hemodinamico_AR1B[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAMB1", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1B[4] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemTAM1 = control_hemodinamico_AR1C[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAM1C", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1C[4] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemTAM1 = control_hemodinamico_AR1D[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAM1D", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1D[4] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemTAM1 = control_hemodinamico_AR1E[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAM1E", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1E[4] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemTAM1 = control_hemodinamico_AR1F[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAM1F", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1F[4] + " SAVED");

        } else if (viewpac7_butpressed === true) {
            var save_conthemTAM1 = control_hemodinamico_AR1G[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAM1G", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1G[4] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemTAM1 = control_hemodinamico_AR1H[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAM1H", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1H[4] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemTAM1 = control_hemodinamico_AR1I[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAM1I", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1I[4] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemTAM1 = control_hemodinamico_AR1J[4];
            var save_conthemTAM1b = JSON.stringify(save_conthemTAM1);
            localStorage.setItem("saved_cont_hemTAM1J", save_conthemTAM1b);
            console.log(control_hemodinamico_AR1J[4] + " SAVED");
        }

    }

    function save_cont_hemTAS2() {

        if (viewpac1_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2[1] + " SAVED");

        } else if (viewpac2_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2B[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2B", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2B[1] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2C[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2C", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2C[1] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2D[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2D", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2D[1] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2E[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2E", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2E[1] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2F[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2F", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2F[1] + " SAVED");

        } else if (viewpac7_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2G[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2G", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2G[1] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2H[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2H", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2H[1] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2I[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2I", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2I[1] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemTAS2 = control_hemodinamico_AR2J[1];
            var save_conthemTAS2b = JSON.stringify(save_conthemTAS2);
            localStorage.setItem("saved_cont_hemTAS2J", save_conthemTAS2b);
            console.log(control_hemodinamico_AR2J[1] + " SAVED");

        }
   
    }

    function save_cont_hemTAD2() {

        if (viewpac1_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2[2] + " SAVED");

        } else if (viewpac2_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2B[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2B", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2B[2] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2C[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2C", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2C[2] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2D[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2D", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2D[2] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2E[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2E", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2E[2] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2F[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2F", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2F[2] + " SAVED");

        } else if (viewpac7_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2G[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2G", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2G[2] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2H[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2H", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2H[2] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2I[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2I", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2I[2] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemTAD2 = control_hemodinamico_AR2J[2];
            var save_conthemTAD2b = JSON.stringify(save_conthemTAD2);
            localStorage.setItem("saved_cont_hemTAD2J", save_conthemTAD2b);
            console.log(control_hemodinamico_AR2J[2] + " SAVED");

        }

    }

    function save_cont_hemFC2() {

        if (viewpac1_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2", save_conthemFC2b);
            console.log(control_hemodinamico_AR2[3] + " SAVED");

        } else if (viewpac2_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2B[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2B", save_conthemFC2b);
            console.log(control_hemodinamico_AR2B[3] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2C[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2C", save_conthemFC2b);
            console.log(control_hemodinamico_AR2C[3] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2D[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2D", save_conthemFC2b);
            console.log(control_hemodinamico_AR2D[3] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2E[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2E", save_conthemFC2b);
            console.log(control_hemodinamico_AR2E[3] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2F[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2F", save_conthemFC2b);
            console.log(control_hemodinamico_AR2F[3] + " SAVED");

        } else if (viewpac7_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2G[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2G", save_conthemFC2b);
            console.log(control_hemodinamico_AR2G[3] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2H[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2H", save_conthemFC2b);
            console.log(control_hemodinamico_AR2H[3] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2I[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2I", save_conthemFC2b);
            console.log(control_hemodinamico_AR2I[3] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemFC2 = control_hemodinamico_AR2J[3];
            var save_conthemFC2b = JSON.stringify(save_conthemFC2);
            localStorage.setItem("saved_cont_hemFC2J", save_conthemFC2b);
            console.log(control_hemodinamico_AR2J[3] + " SAVED");

        }
 

    }

    function save_cont_hemTAM2() {

        if (viewpac1_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        } else if (viewpac2_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        } else if (viewpac7_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemTAM2 = control_hemodinamico_AR2[4];
            var save_conthemTAM2b = JSON.stringify(save_conthemTAM2);
            localStorage.setItem("saved_cont_hemTAM2", save_conthemTAM2b);
            console.log(control_hemodinamico_AR2[4] + " SAVED");

        }


    }

    function save_cont_hemTAS3() {

        if (viewpac1_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3[1] + " SAVED");

        } else if (viewpac2_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3B[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3B", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3B[1] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3C[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3C", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3C[1] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3D[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3D", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3D[1] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3E[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3E", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3E[1] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3F[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3F", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3F[1] + " SAVED");

        } else if (viewpac7_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3G[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3G", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3G[1] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3H[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3H", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3H[1] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3I[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3I", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3I[1] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemTAS3 = control_hemodinamico_AR3J[1];
            var save_conthemTAS3b = JSON.stringify(save_conthemTAS3);
            localStorage.setItem("saved_cont_hemTAS3J", save_conthemTAS3b);
            console.log(control_hemodinamico_AR3J[1] + " SAVED");

        }
     
    }

    function save_cont_hemTAD3() {

        if (viewpac1_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3[2] + " SAVED");

        } else if (viewpac2_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3B[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3B", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3B[2] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3C[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3C", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3C[2] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3D[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3D", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3D[2] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3E[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3E", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3E[2] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3F[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3F", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3F[2] + " SAVED");
                
        } else if (viewpac7_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3G[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3G", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3G[2] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3H[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3H", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3H[2] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3I[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3I", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3I[2] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemTAD3 = control_hemodinamico_AR3J[2];
            var save_conthemTAD3b = JSON.stringify(save_conthemTAD3);
            localStorage.setItem("saved_cont_hemTAD3J", save_conthemTAD3b);
            console.log(control_hemodinamico_AR3J[2] + " SAVED");

        }

    }


    function save_cont_hemFC3() {

        if (viewpac1_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3", save_conthemFC3b);
            console.log(control_hemodinamico_AR3[3] + " SAVED");

        } else if (viewpac2_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3B[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3B", save_conthemFC3b);
            console.log(control_hemodinamico_AR3B[3] + " SAVED");

        } else if (viewpac3_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3C[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3C", save_conthemFC3b);
            console.log(control_hemodinamico_AR3C[3] + " SAVED");

        } else if (viewpac4_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3D[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3D", save_conthemFC3b);
            console.log(control_hemodinamico_AR3D[3] + " SAVED");

        } else if (viewpac5_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3E[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3E", save_conthemFC3b);
            console.log(control_hemodinamico_AR3E[3] + " SAVED");

        } else if (viewpac6_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3F[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3F", save_conthemFC3b);
            console.log(control_hemodinamico_AR3F[3] + " SAVED");

        } else if (viewpac7_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3G[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3G", save_conthemFC3b);
            console.log(control_hemodinamico_AR3G[3] + " SAVED");

        } else if (viewpac8_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3H[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3H", save_conthemFC3b);
            console.log(control_hemodinamico_AR3H[3] + " SAVED");

        } else if (viewpac9_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3I[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3I", save_conthemFC3b);
            console.log(control_hemodinamico_AR3I[3] + " SAVED");

        } else if (viewpac10_butpressed === true) {
            var save_conthemFC3 = control_hemodinamico_AR3J[3];
            var save_conthemFC3b = JSON.stringify(save_conthemFC3);
            localStorage.setItem("saved_cont_hemFC3J", save_conthemFC3b);
            console.log(control_hemodinamico_AR3J[3] + " SAVED");
        }
   

    }

//continuar a partir de aca
    function save_cont_hemTAM3() {
        var save_conthemTAM3 = control_hemodinamico_AR3[4];
        var save_conthemTAM3b = JSON.stringify(save_conthemTAM3);
        localStorage.setItem("saved_cont_hemTAM3", save_conthemTAM3b);
        console.log(control_hemodinamico_AR3[4] + " SAVED");

    }

    function save_cont_hemTAS4() {
        var save_conthemTAS4 = control_hemodinamico_AR4[1];
        var save_conthemTAS4b = JSON.stringify(save_conthemTAS4);
        localStorage.setItem("saved_cont_hemTAS4", save_conthemTAS4b);
        console.log(control_hemodinamico_AR4[1] + " SAVED");
    }

    function save_cont_hemTAD4() {
        var save_conthemTAD4 = control_hemodinamico_AR4[2];
        var save_conthemTAD4b = JSON.stringify(save_conthemTAD4);
        localStorage.setItem("saved_cont_hemTAD4", save_conthemTAD4b);
        console.log(control_hemodinamico_AR4[2] + " SAVED");
    }


    function save_cont_hemFC4() {
        var save_conthemFC4 = control_hemodinamico_AR4[3];
        var save_conthemFC4b = JSON.stringify(save_conthemFC4);
        localStorage.setItem("saved_cont_hemFC4", save_conthemFC4b);
        console.log(control_hemodinamico_AR4[3] + " SAVED");

    }

    function save_cont_hemTAM4() {
        var save_conthemTAM4 = control_hemodinamico_AR4[4];
        var save_conthemTAM4b = JSON.stringify(save_conthemTAM4);
        localStorage.setItem("saved_cont_hemTAM4", save_conthemTAM4b);
        console.log(control_hemodinamico_AR4[4] + " SAVED");

    }

    function save_cont_hemTAS5() {
        var save_conthemTAS5 = control_hemodinamico_AR5[1];
        var save_conthemTAS5b = JSON.stringify(save_conthemTAS5);
        localStorage.setItem("saved_cont_hemTAS5", save_conthemTAS5b);
        console.log(control_hemodinamico_AR5[1] + " SAVED");
    }

    function save_cont_hemTAD5() {
        var save_conthemTAD5 = control_hemodinamico_AR5[2];
        var save_conthemTAD5b = JSON.stringify(save_conthemTAD5);
        localStorage.setItem("saved_cont_hemTAD5", save_conthemTAD5b);
        console.log(control_hemodinamico_AR5[2] + " SAVED");
    }


    function save_cont_hemFC5() {
        var save_conthemFC5 = control_hemodinamico_AR5[3];
        var save_conthemFC5b = JSON.stringify(save_conthemFC5);
        localStorage.setItem("saved_cont_hemFC5", save_conthemFC5b);
        console.log(control_hemodinamico_AR5[3] + " SAVED");

    }

    function save_cont_hemTAM5() {
        var save_conthemTAM5 = control_hemodinamico_AR5[4];
        var save_conthemTAM5b = JSON.stringify(save_conthemTAM5);
        localStorage.setItem("saved_cont_hemTAM5", save_conthemTAM5b);
        console.log(control_hemodinamico_AR5[4] + " SAVED");

    }

    // load control hemodinamico AR from local storage
    function load_cont_hemTAS1() {

        if (viewpac1_butpressed === true) {

            var load_conthemTAS1 = localStorage.getItem("saved_cont_hemTAS1");
            console.log(load_conthemTAS1);
            var load_conthemTAS1b = JSON.parse(load_conthemTAS1);
            console.log(load_conthemTAS1b);
            control_hemodinamico_AR1[1] = load_conthemTAS1b;
            console.log(control_hemodinamico_AR1[1] + " LOADED");

            var tasval = document.getElementById("TASval1");

            if (tasval) {
                document.getElementById("TASval1").innerHTML = control_hemodinamico_AR1[1];

            }

        } else if (viewpac2_butpressed === true) {

            var load_conthemTAS1 = localStorage.getItem("saved_cont_hemTAS1B");
            console.log(load_conthemTAS1);
            var load_conthemTAS1b = JSON.parse(load_conthemTAS1);
            console.log(load_conthemTAS1b);
            control_hemodinamico_AR1B[1] = load_conthemTAS1b;
            console.log(control_hemodinamico_AR1B[1] + " LOADED");

            var tasval = document.getElementById("TASval1");

            if (tasval) {
                document.getElementById("TASval1").innerHTML = control_hemodinamico_AR1B[1];

            }

        } else if (viewpac3_butpressed === true) {

            var load_conthemTAS1 = localStorage.getItem("saved_cont_hemTAS1C");
            console.log(load_conthemTAS1);
            var load_conthemTAS1b = JSON.parse(load_conthemTAS1);
            console.log(load_conthemTAS1b);
            control_hemodinamico_AR1C[1] = load_conthemTAS1b;
            console.log(control_hemodinamico_AR1C[1] + " LOADED");

            var tasval = document.getElementById("TASval1");

            if (tasval) {
                document.getElementById("TASval1").innerHTML = control_hemodinamico_AR1C[1];

            }

        } else if (viewpac4_butpressed === true) {

            var load_conthemTAS1 = localStorage.getItem("saved_cont_hemTAS1D");
            console.log(load_conthemTAS1);
            var load_conthemTAS1b = JSON.parse(load_conthemTAS1);
            console.log(load_conthemTAS1b);
            control_hemodinamico_AR1D[1] = load_conthemTAS1b;
            console.log(control_hemodinamico_AR1D[1] + " LOADED");

            var tasval = document.getElementById("TASval1");

            if (tasval) {
                document.getElementById("TASval1").innerHTML = control_hemodinamico_AR1D[1];

            }

        } else {
            alert("load_cont_hem_TAS1 ERROR");
            console.log("ERROR");
               }

    
    }

    function load_cont_hemTAD1() {

        if (viewpac1_butpressed === true) {
            var load_conthemTAD1 = localStorage.getItem("saved_cont_hemTAD1");
            console.log(load_conthemTAD1);
            var load_conthemTAD1b = JSON.parse(load_conthemTAD1);
            console.log(load_conthemTAD1b);
            control_hemodinamico_AR1[2] = load_conthemTAD1b;
            console.log(control_hemodinamico_AR1[1] + " LOADED");

            var tadval = document.getElementById("TADval1");

            if (tadval) {
                document.getElementById("TADval1").innerHTML = control_hemodinamico_AR1[2];
            }
        } else if (viewpac2_butpressed === true) {
            var load_conthemTAD1 = localStorage.getItem("saved_cont_hemTAD1B");
            console.log(load_conthemTAD1);
            var load_conthemTAD1b = JSON.parse(load_conthemTAD1);
            console.log(load_conthemTAD1b);
            control_hemodinamico_AR1B[2] = load_conthemTAD1b;
            console.log(control_hemodinamico_AR1B[1] + " LOADED");

            var tadval = document.getElementById("TADval1");

            if (tadval) {
                document.getElementById("TADval1").innerHTML = control_hemodinamico_AR1B[2];
            }
        }
 
    }

    function load_cont_hemFC1() {

        if (viewpac1_butpressed === true) {
            var load_conthemFC1 = localStorage.getItem("saved_cont_hemFC1");
            console.log(load_conthemFC1);
            var load_conthemFC1b = JSON.parse(load_conthemFC1);
            console.log(load_conthemFC1b);
            control_hemodinamico_AR1[3] = load_conthemFC1b;
            console.log(control_hemodinamico_AR1[3] + " LOADED");

            var fcval = document.getElementById("fcval1");

            if (fcval) {
                document.getElementById("fcval1").innerHTML = control_hemodinamico_AR1[3];
            }
        } else if (viewpac2_butpressed === true) {
            var load_conthemFC1 = localStorage.getItem("saved_cont_hemFC1B");
            console.log(load_conthemFC1);
            var load_conthemFC1b = JSON.parse(load_conthemFC1);
            console.log(load_conthemFC1b);
            control_hemodinamico_AR1B[3] = load_conthemFC1b;
            console.log(control_hemodinamico_AR1B[3] + " LOADED");

            var fcval = document.getElementById("fcval1");

            if (fcval) {
                document.getElementById("fcval1").innerHTML = control_hemodinamico_AR1B[3];
            }
        }
 
    }

    function load_cont_hemTAM1() {

        if (viewpac1_butpressed === true) {
            var load_conthemTAM1 = localStorage.getItem("saved_cont_hemTAM1");
            console.log(load_conthemTAM1);
            var load_conthemTAM1b = JSON.parse(load_conthemTAM1);
            console.log(load_conthemTAM1b);
            control_hemodinamico_AR1[4] = load_conthemTAM1b;
            console.log(control_hemodinamico_AR1[4] + " LOADED");

            var tamval = document.getElementById("tamval1");

            if (tamval) {

                document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1[4];
            }
        } else if (viewpac2_butpressed === true) {
            var load_conthemTAM1 = localStorage.getItem("saved_cont_hemTAM1B");
            console.log(load_conthemTAM1);
            var load_conthemTAM1b = JSON.parse(load_conthemTAM1);
            console.log(load_conthemTAM1b);
            control_hemodinamico_AR1B[4] = load_conthemTAM1b;
            console.log(control_hemodinamico_AR1B[4] + " LOADED");

            var tamval = document.getElementById("tamval1");

            if (tamval) {

                document.getElementById("tamval1").innerHTML = control_hemodinamico_AR1B[4];
            }
        }
    
    }

    function load_cont_hemTAS2() {

        var load_conthemTAS2 = localStorage.getItem("saved_cont_hemTAS2");
        console.log(load_conthemTAS2);
        var load_conthemTAS2b = JSON.parse(load_conthemTAS2);
        console.log(load_conthemTAS2b);
        control_hemodinamico_AR2[1] = load_conthemTAS2b;
        console.log(control_hemodinamico_AR2[1] + " LOADED");

        var tasval = document.getElementById("TASval2");

        if (tasval) {

            document.getElementById("TASval2").innerHTML = control_hemodinamico_AR2[1];
        }

    }

    function load_cont_hemTAD2() {

        var load_conthemTAD2 = localStorage.getItem("saved_cont_hemTAD2");
        console.log(load_conthemTAD2);
        var load_conthemTAD2b = JSON.parse(load_conthemTAD2);
        console.log(load_conthemTAD2b);
        control_hemodinamico_AR2[2] = load_conthemTAD2b;
        console.log(control_hemodinamico_AR2[1] + " LOADED");

        var tadval = document.getElementById("TADval2");

        if (tadval) {

            document.getElementById("TADval2").innerHTML = control_hemodinamico_AR2[2];
        }
    }


    function load_cont_hemFC2() {

        var load_conthemFC2 = localStorage.getItem("saved_cont_hemFC2");
        console.log(load_conthemFC2);
        var load_conthemFC2b = JSON.parse(load_conthemFC2);
        console.log(load_conthemFC2b);
        control_hemodinamico_AR2[3] = load_conthemFC2b;
        console.log(control_hemodinamico_AR2[3] + " LOADED");

        var fcval = document.getElementById("fcval2");

        if (fcval) {

            document.getElementById("fcval2").innerHTML = control_hemodinamico_AR2[3];
        }
    }

    function load_cont_hemTAM2() {
        var load_conthemTAM2 = localStorage.getItem("saved_cont_hemTAM2");
        console.log(load_conthemTAM2);
        var load_conthemTAM2b = JSON.parse(load_conthemTAM2);
        console.log(load_conthemTAM2b);
        control_hemodinamico_AR2[4] = load_conthemTAM2b;
        console.log(control_hemodinamico_AR2[4] + " LOADED");

        var tamval = document.getElementById("tamval2");

        if (tamval) {

            document.getElementById("tamval2").innerHTML = control_hemodinamico_AR2[4];
        }
    }

    function load_cont_hemTAS3() {

        var load_conthemTAS3 = localStorage.getItem("saved_cont_hemTAS3");
        console.log(load_conthemTAS3);
        var load_conthemTAS3b = JSON.parse(load_conthemTAS3);
        console.log(load_conthemTAS3b);
        control_hemodinamico_AR3[1] = load_conthemTAS3b;
        console.log(control_hemodinamico_AR3[1] + " LOADED");

        var tasval = document.getElementById("TASval3");

        if (tasval) {

            document.getElementById("TASval3").innerHTML = control_hemodinamico_AR3[1];
        }

    }

    function load_cont_hemTAD3() {

        var load_conthemTAD3 = localStorage.getItem("saved_cont_hemTAD3");
        console.log(load_conthemTAD3);
        var load_conthemTAD3b = JSON.parse(load_conthemTAD3);
        console.log(load_conthemTAD3b);
        control_hemodinamico_AR3[2] = load_conthemTAD3b;
        console.log(control_hemodinamico_AR3[1] + " LOADED");

        var tadval = document.getElementById("TADval3");

        if (tadval) {

            document.getElementById("TADval3").innerHTML = control_hemodinamico_AR3[2];
        }
    }

    function load_cont_hemFC3() {

        var load_conthemFC3 = localStorage.getItem("saved_cont_hemFC3");
        console.log(load_conthemFC3);
        var load_conthemFC3b = JSON.parse(load_conthemFC3);
        console.log(load_conthemFC3b);
        control_hemodinamico_AR3[3] = load_conthemFC3b;
        console.log(control_hemodinamico_AR3[3] + " LOADED");

        var fcval = document.getElementById("fcval3");

        if (fcval) {

            document.getElementById("fcval3").innerHTML = control_hemodinamico_AR3[3];
        }
    }

    function load_cont_hemTAM3() {
        var load_conthemTAM3 = localStorage.getItem("saved_cont_hemTAM3");
        console.log(load_conthemTAM3);
        var load_conthemTAM3b = JSON.parse(load_conthemTAM3);
        console.log(load_conthemTAM3b);
        control_hemodinamico_AR3[4] = load_conthemTAM3b;
        console.log(control_hemodinamico_AR3[4] + " LOADED");

        var tamval = document.getElementById("tamval3");

        if (tamval) {

            document.getElementById("tamval3").innerHTML = control_hemodinamico_AR3[4];
        }
    }

    function load_cont_hemTAS4() {

        var load_conthemTAS4 = localStorage.getItem("saved_cont_hemTAS4");
        console.log(load_conthemTAS4);
        var load_conthemTAS4b = JSON.parse(load_conthemTAS4);
        console.log(load_conthemTAS4b);
        control_hemodinamico_AR4[1] = load_conthemTAS4b;
        console.log(control_hemodinamico_AR4[1] + " LOADED");

        var tasval = document.getElementById("TASval4");

        if (tasval) {

            document.getElementById("TASval4").innerHTML = control_hemodinamico_AR4[1];
        }

    }

    function load_cont_hemTAD4() {

        var load_conthemTAD4 = localStorage.getItem("saved_cont_hemTAD4");
        console.log(load_conthemTAD4);
        var load_conthemTAD4b = JSON.parse(load_conthemTAD4);
        console.log(load_conthemTAD4b);
        control_hemodinamico_AR4[2] = load_conthemTAD4b;
        console.log(control_hemodinamico_AR4[1] + " LOADED");

        var tadval = document.getElementById("TADval4");

        if (tadval) {

            document.getElementById("TADval4").innerHTML = control_hemodinamico_AR4[2];
        }
    }

    function load_cont_hemFC4() {

        var load_conthemFC4 = localStorage.getItem("saved_cont_hemFC4");
        console.log(load_conthemFC4);
        var load_conthemFC4b = JSON.parse(load_conthemFC4);
        console.log(load_conthemFC4b);
        control_hemodinamico_AR4[3] = load_conthemFC4b;
        console.log(control_hemodinamico_AR4[3] + " LOADED");

        var fcval = document.getElementById("fcval4");

        if (fcval) {

            document.getElementById("fcval4").innerHTML = control_hemodinamico_AR4[3];
        }
    }

    function load_cont_hemTAM4() {
        var load_conthemTAM4 = localStorage.getItem("saved_cont_hemTAM4");
        console.log(load_conthemTAM4);
        var load_conthemTAM4b = JSON.parse(load_conthemTAM4);
        console.log(load_conthemTAM4b);
        control_hemodinamico_AR4[4] = load_conthemTAM4b;
        console.log(control_hemodinamico_AR4[4] + " LOADED");

        var tamval = document.getElementById("tamval4");

        if (tamval) {

            document.getElementById("tamval4").innerHTML = control_hemodinamico_AR4[4];
        }
    }

    function load_cont_hemTAS5() {

        var load_conthemTAS5 = localStorage.getItem("saved_cont_hemTAS5");
        console.log(load_conthemTAS5);
        var load_conthemTAS5b = JSON.parse(load_conthemTAS5);
        console.log(load_conthemTAS5b);
        control_hemodinamico_AR5[1] = load_conthemTAS5b;
        console.log(control_hemodinamico_AR5[1] + " LOADED");

        var tasval = document.getElementById("TASval5");

        if (tasval) {

            document.getElementById("TASval5").innerHTML = control_hemodinamico_AR5[1];
        }

    }

    function load_cont_hemTAD5() {

        var load_conthemTAD5 = localStorage.getItem("saved_cont_hemTAD5");
        console.log(load_conthemTAD5);
        var load_conthemTAD5b = JSON.parse(load_conthemTAD5);
        console.log(load_conthemTAD5b);
        control_hemodinamico_AR5[2] = load_conthemTAD5b;
        console.log(control_hemodinamico_AR5[1] + " LOADED");

        var tadval = document.getElementById("TADval5");

        if (tadval) {

            document.getElementById("TADval5").innerHTML = control_hemodinamico_AR5[2];
        }
    }

    function load_cont_hemFC5() {

        var load_conthemFC5 = localStorage.getItem("saved_cont_hemFC5");
        console.log(load_conthemFC5);
        var load_conthemFC5b = JSON.parse(load_conthemFC5);
        console.log(load_conthemFC5b);
        control_hemodinamico_AR5[3] = load_conthemFC5b;
        console.log(control_hemodinamico_AR5[3] + " LOADED");

        var fcval = document.getElementById("fcval5");

        if (fcval) {

            document.getElementById("fcval5").innerHTML = control_hemodinamico_AR5[3];
        }
    }

    function load_cont_hemTAM5() {
        var load_conthemTAM5 = localStorage.getItem("saved_cont_hemTAM5");
        console.log(load_conthemTAM5);
        var load_conthemTAM5b = JSON.parse(load_conthemTAM5);
        console.log(load_conthemTAM5b);
        control_hemodinamico_AR5[4] = load_conthemTAM5b;
        console.log(control_hemodinamico_AR5[4] + " LOADED");

        var tamval = document.getElementById("tamval5");

        if (tamval) {

            document.getElementById("tamval5").innerHTML = control_hemodinamico_AR5[4];
        }
    }


// CONTINUAR DE ACA PARA ARRIBA

    //RELOJ
    function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('txt').innerHTML =
            h + ":" + m + ":" + s;
        var t = setTimeout(startTime, 500);

        get_h = h;
        get_m = m;

    }
    function checkTime(i) {
        if (i < 10) { i = "0" + i };  // add zero in front of numbers < 10
        return i;
    }

    function adddata_topage() {//carga la ficha del paciente

        //load which ver pac buton is clicked

        //pac1
        var load_verpac1but = localStorage.getItem("strd_pacbu1");
        var load_verpac1butb = JSON.parse(load_verpac1but);
        viewpac1_butpressed = load_verpac1butb;
        console.log(viewpac1_butpressed + " paciente 1");
        //pac2
        var load_verpac2but = localStorage.getItem("strd_pacbu2");
        var load_verpac2butb = JSON.parse(load_verpac2but);
        viewpac2_butpressed = load_verpac2butb;
        console.log(viewpac2_butpressed + " paciente 2");
        //pac3
        var load_verpac3but = localStorage.getItem("strd_pacbu3");
        var load_verpac3butb = JSON.parse(load_verpac3but);
        viewpac3_butpressed = load_verpac3butb;
        console.log(viewpac3_butpressed + " paciente 3");
        //pac4
        var load_verpac4but = localStorage.getItem("strd_pacbu4");
        var load_verpac4butb = JSON.parse(load_verpac4but);
        viewpac4_butpressed = load_verpac4butb;
        console.log(viewpac4_butpressed + " paciente 4");
        //pac5
        var load_verpac5but = localStorage.getItem("strd_pacbu5");
        var load_verpac5butb = JSON.parse(load_verpac5but);
        viewpac5_butpressed = load_verpac5butb;
        console.log(viewpac5_butpressed + " paciente 5");
        //pac6
        var load_verpac6but = localStorage.getItem("strd_pacbu6");
        var load_verpac6butb = JSON.parse(load_verpac6but);
        viewpac6_butpressed = load_verpac6butb;
        console.log(viewpac6_butpressed + " paciente 6");
        //pac7
        var load_verpac7but = localStorage.getItem("strd_pacbu7");
        var load_verpac7butb = JSON.parse(load_verpac7but);
        viewpac7_butpressed = load_verpac7butb;
        console.log(viewpac7_butpressed + " paciente 7");
        //pac8
        var load_verpac8but = localStorage.getItem("strd_pacbu8");
        var load_verpac8butb = JSON.parse(load_verpac8but);
        viewpac8_butpressed = load_verpac8butb;
        console.log(viewpac8_butpressed + " paciente 8");
        //pac9
        var load_verpac9but = localStorage.getItem("strd_pacbu9");
        var load_verpac9butb = JSON.parse(load_verpac9but);
        viewpac9_butpressed = load_verpac9butb;
        console.log(viewpac9_butpressed + " paciente 9");
        //pac10
        var load_verpac10but = localStorage.getItem("strd_pacbu10");
        var load_verpac10butb = JSON.parse(load_verpac10but);
        viewpac10_butpressed = load_verpac10butb;
        console.log(viewpac10_butpressed + " paciente 10");



        if (viewpac1_butpressed === true) {
            var adddata_logp0_cama = Log_p0[0];
            document.getElementById("cama").innerHTML = adddata_logp0_cama;

            var adddata_logp0_apellido = Log_p0[1];
            document.getElementById("apellido").innerHTML = adddata_logp0_apellido;

            var adddata_logp0_sexo = Log_p0[2];
            document.getElementById("sexo").innerHTML = adddata_logp0_sexo;
        } else if (viewpac2_butpressed === true) {
            var adddata_logp1_cama = Log_p1[0];
            document.getElementById("cama").innerHTML = adddata_logp1_cama;

            var adddata_logp1_apellido = Log_p1[1];
            document.getElementById("apellido").innerHTML = adddata_logp1_apellido;

            var adddata_logp1_sexo = Log_p1[2];
            document.getElementById("sexo").innerHTML = adddata_logp1_sexo;
        } else if (viewpac3_butpressed === true) {
            var adddata_logp2_cama = Log_p2[0];
            document.getElementById("cama").innerHTML = adddata_logp2_cama;

            var adddata_logp2_apellido = Log_p2[1];
            document.getElementById("apellido").innerHTML = adddata_logp2_apellido;

            var adddata_logp2_sexo = Log_p2[2];
            document.getElementById("sexo").innerHTML = adddata_logp2_sexo;
        } else if (viewpac4_butpressed === true) {
            var adddata_logp3_cama = Log_p3[0];
            document.getElementById("cama").innerHTML = adddata_logp3_cama;

            var adddata_logp3_apellido = Log_p3[1];
            document.getElementById("apellido").innerHTML = adddata_logp3_apellido;

            var adddata_logp3_sexo = Log_p3[2];
            document.getElementById("sexo").innerHTML = adddata_logp3_sexo;
        } else if (viewpac5_butpressed === true) {
            var adddata_logp4_cama = Log_p4[0];
            document.getElementById("cama").innerHTML = adddata_logp4_cama;

            var adddata_logp4_apellido = Log_p4[1];
            document.getElementById("apellido").innerHTML = adddata_logp4_apellido;

            var adddata_logp4_sexo = Log_p4[2];
            document.getElementById("sexo").innerHTML = adddata_logp4_sexo;
        } else if (viewpac6_butpressed === true) {
            var adddata_logp5_cama = Log_p5[0];
            document.getElementById("cama").innerHTML = adddata_logp5_cama;

            var adddata_logp5_apellido = Log_p5[1];
            document.getElementById("apellido").innerHTML = adddata_logp5_apellido;

            var adddata_logp5_sexo = Log_p5[2];
            document.getElementById("sexo").innerHTML = adddata_logp5_sexo;
        } else if (viewpac7_butpressed === true) {
            var adddata_logp6_cama = Log_p6[0];
            document.getElementById("cama").innerHTML = adddata_logp6_cama;

            var adddata_logp6_apellido = Log_p6[1];
            document.getElementById("apellido").innerHTML = adddata_logp6_apellido;

            var adddata_logp6_sexo = Log_p6[2];
            document.getElementById("sexo").innerHTML = adddata_logp6_sexo;
        } else if (viewpac8_butpressed === true) {
            var adddata_logp7_cama = Log_p7[0];
            document.getElementById("cama").innerHTML = adddata_logp7_cama;

            var adddata_logp7_apellido = Log_p7[1];
            document.getElementById("apellido").innerHTML = adddata_logp7_apellido;

            var adddata_logp7_sexo = Log_p7[2];
            document.getElementById("sexo").innerHTML = adddata_logp7_sexo;
        } else if (viewpac9_butpressed === true) {
            var adddata_logp8_cama = Log_p8[0];
            document.getElementById("cama").innerHTML = adddata_logp8_cama;

            var adddata_logp8_apellido = Log_p8[1];
            document.getElementById("apellido").innerHTML = adddata_logp8_apellido;

            var adddata_logp8_sexo = Log_p8[2];
            document.getElementById("sexo").innerHTML = adddata_logp8_sexo;
        } else if (viewpac10_butpressed === true) {
            var adddata_logp9_cama = Log_p9[0];
            document.getElementById("cama").innerHTML = adddata_logp9_cama;

            var adddata_logp9_apellido = Log_p9[1];
            document.getElementById("apellido").innerHTML = adddata_logp9_apellido;

            var adddata_logp9_sexo = Log_p9[2];
            document.getElementById("sexo").innerHTML = adddata_logp9_sexo;
        }

    }

function viewing_patient() {
    if (viewpac1_butpressed === true) {

        load_cont_hemTAS1();
        load_cont_hemTAD1();
        load_cont_hemFC1();
        load_cont_hemTAM1();

        load_cont_hemTAS2();
        load_cont_hemTAD2();
        load_cont_hemFC2();
        load_cont_hemTAM2();

        load_cont_hemTAS3();
        load_cont_hemTAD3();
        load_cont_hemFC3();
        load_cont_hemTAM3();

        load_cont_hemTAS4();
        load_cont_hemTAD4();
        load_cont_hemFC4();
        load_cont_hemTAM4();

        load_cont_hemTAS5();
        load_cont_hemTAD5();
        load_cont_hemFC5();
        load_cont_hemTAM5();

    } else if (viewpac2_butpressed === true) {

        load_cont_hemTAS1();
        load_cont_hemTAD1();
        load_cont_hemFC1();
        load_cont_hemTAM1();

        load_cont_hemTAS2();
        load_cont_hemTAD2();
        load_cont_hemFC2();
        load_cont_hemTAM2();

        load_cont_hemTAS3();
        load_cont_hemTAD3();
        load_cont_hemFC3();
        load_cont_hemTAM3();

        load_cont_hemTAS4();
        load_cont_hemTAD4();
        load_cont_hemFC4();
        load_cont_hemTAM4();

        load_cont_hemTAS5();
        load_cont_hemTAD5();
        load_cont_hemFC5();
        load_cont_hemTAM5();

    } else if (viewpac3_butpressed === true) {

        load_cont_hemTAS1();
        load_cont_hemTAD1();
        load_cont_hemFC1();
        load_cont_hemTAM1();

        load_cont_hemTAS2();
        load_cont_hemTAD2();
        load_cont_hemFC2();
        load_cont_hemTAM2();

        load_cont_hemTAS3();
        load_cont_hemTAD3();
        load_cont_hemFC3();
        load_cont_hemTAM3();

        load_cont_hemTAS4();
        load_cont_hemTAD4();
        load_cont_hemFC4();
        load_cont_hemTAM4();

        load_cont_hemTAS5();
        load_cont_hemTAD5();
        load_cont_hemFC5();
        load_cont_hemTAM5();

    } else {
        alert("error");
        console.log("ERROR");
            }
}



function respiratorio_butpressed() {
    set_controlnd();
    console.log(control_nd);
    
    create_respiratorio();

    mon_respi();
}


//CONTROL RESPIRATORIO 

function create_respiratorio() {

    if (click_check === false) {
        var div_controles = document.getElementById("controles");

        var cr_controlrespi = document.createElement("div");
        var cr_row1 = document.createElement("div");
        var cr_row1 = document.createElement("div");
        var cr_col1 = document.createElement("div");
        var cr_col2 = document.createElement("div");
        var cr_col3 = document.createElement("div");
        var cr_accordion4 = document.createElement("div");
        var cr_card = document.createElement("div");
        var cr_cardheader = document.createElement("div");
        var cr_precarddiv = document.createElement("div");
        var cr_cardbody = document.createElement("div");
        var cr_fr_formcontrol = document.createElement("div");
        var cr_sat_formcontrol = document.createElement("div");
        var cr_h5 = document.createElement("h5");
        var cr_but1 = document.createElement("button");
        var cr_but_addfr = document.createElement("button");
        var cr_butclose = document.createElement("button");
        var cr_but_addsat = document.createElement("button");
        var cr_title1 = document.createElement("p");
        var cr_frtitle = document.createElement("p");
        var cr_sattitle = document.createElement("p");
        var cr_frvalue = document.createElement("p");
        var cr_satvalue = document.createElement("p");

        div_controles.appendChild(cr_accordion4);
        cr_accordion4.setAttribute("id", "accordion4");

        cr_card.appendChild(cr_row1);
        cr_row1.setAttribute("class", "row");

        cr_row1.appendChild(cr_col1);
        cr_col1.setAttribute("class", "col-sm");

        cr_row1.appendChild(cr_col2);
        cr_col2.setAttribute("class", "col-sm");

        cr_row1.appendChild(cr_col3);
        cr_col3.setAttribute("class", "col-sm");

        cr_col3.appendChild(cr_butclose);
        cr_butclose.setAttribute("class", "btn btn-secondary btn-sm float-right");
        cr_butclose.innerHTML = "x";

        cr_accordion4.appendChild(cr_card);
        cr_card.setAttribute("class", "card");

        cr_card.appendChild(cr_cardheader);
        cr_cardheader.setAttribute("class", "card-header");
        cr_cardheader.setAttribute("id", "headingFour");

        cr_cardheader.appendChild(cr_h5);
        cr_h5.setAttribute("class", "mb-0");

        cr_h5.appendChild(cr_but1);
        cr_but1.setAttribute("class", "btn btn-primary");
        cr_but1.setAttribute("data-toggle", "collapse");
        cr_but1.setAttribute("data-target", "#collapseFour");
        cr_but1.setAttribute("aria-expanded", "true");
        cr_but1.setAttribute("aria-controls", "collapseFour");
        cr_but1.innerHTML = "Respiratorio";

        cr_card.appendChild(cr_precarddiv);
        cr_precarddiv.setAttribute("id", "collapseFour");
        cr_precarddiv.setAttribute("class", "collapse show");
        cr_precarddiv.setAttribute("aria-labelledby", "headingFour");
        cr_precarddiv.setAttribute("data-parent", "#accordion4");

        cr_precarddiv.appendChild(cr_cardbody);
        cr_cardbody.setAttribute("class", "card-body");

        cr_cardbody.appendChild(cr_title1);
        cr_title1.setAttribute("class", "h2 text-center");
        cr_title1.innerHTML = "Control Respiratorio";

        cr_cardbody.appendChild(cr_fr_formcontrol);
        cr_fr_formcontrol.setAttribute("class", "form-control text-center");

        cr_fr_formcontrol.appendChild(cr_frtitle);
        cr_frtitle.setAttribute("class", "h3 text-center");
        cr_frtitle.innerHTML = "Frecuencia Respiratoria";

        cr_fr_formcontrol.appendChild(cr_frvalue);
        cr_frvalue.setAttribute("class", "h3 text-center");
        cr_frvalue.setAttribute("id", "frvalue");
        cr_frvalue.innerHTML = "fr?";

        cr_fr_formcontrol.appendChild(cr_but_addfr);
        cr_but_addfr.setAttribute("class", "btn btn-primary");
        cr_but_addfr.setAttribute("id", "but_addfr");
        cr_but_addfr.innerHTML = "Agregar frecuencia respiratoria";

        cr_cardbody.appendChild(cr_sat_formcontrol);
        cr_sat_formcontrol.setAttribute("class", "form-control text-center");

        cr_sat_formcontrol.appendChild(cr_sattitle);
        cr_sattitle.setAttribute("class", "h3 text-center");
        cr_sattitle.innerHTML = "Saturacion O2";

        cr_sat_formcontrol.appendChild(cr_satvalue);
        cr_satvalue.setAttribute("class", "h3");
        cr_satvalue.setAttribute("id", "satvalue");
        cr_satvalue.innerHTML = "sat?";

        cr_sat_formcontrol.appendChild(cr_but_addsat);
        cr_but_addsat.setAttribute("class", "btn btn-primary");
        cr_but_addsat.setAttribute("id", "but_addsat");
        cr_but_addsat.innerHTML = "Agregar saturacion de O2";

    }

 

        click_check = true;
  

    

}

//control_nd respiratorio

function set_controlnd() {
    if (control_nd === null) {
        console.log("null");
        control_nd = 0;
    } else if (control_nd === 0) {
        var sum_controlnd = control_nd + 1;
        control_nd = sum_controlnd;
        console.log("control_nd is " + control_nd);

        //save controlnd

        var savend = control_nd;
        var savend2 = JSON.stringify(savend);
        localStorage.setItem("save_controlnd_p1", savend2);

    } else if (control_nd === 1) {
        var sum_controlnd = control_nd + 1;
        control_nd = sum_controlnd;
        console.log("control_nd is " + control_nd);


        var savend = control_nd;
        var savend2 = JSON.stringify(savend);
        localStorage.setItem("save_controlnd_p1", savend2);

    } else if (control_nd === 2) {
        console.log("control_nd is " + control_nd);
        control_nd = 1;


        var savend = control_nd;
        var savend2 = JSON.stringify(savend);
        localStorage.setItem("save_controlnd_p1", savend2);
    }
     
}



//DELETE SLECTED LOCAL STORAGE

function delete_balance() {

    if (viewpac1_butpressed === true) {
        localStorage.removeItem("save_balance_AR1_p1");
        window.location.reload(); //refresh page
    } else if (viewpac2_butpressed === true) {
        localStorage.removeItem("save_balance_AR1_p2");
        window.location.reload(); //refresh page
    } else if (viewpac3_butpressed === true) {
        localStorage.removeItem("save_balance_AR1_p3");
        window.location.reload(); //refresh page
    } else if (viewpac4_butpressed === true) {
        localStorage.removeItem("save_balance_AR1_p4");
        window.location.reload(); //refresh page
    } else if (viewpac5_butpressed === true) {
        localStorage.removeItem("save_balance_AR1_p5");
        window.location.reload(); //refresh page
    } else if (viewpac6_butpressed === true) {
        localStorage.removeItem("save_balance_AR1_p6");
        window.location.reload(); //refresh page
    } else if (viewpac7_butpressed === true) {
        localStorage.removeItem("save_balance_AR1_p7");
        window.location.reload(); //refresh page
    } else if (viewpac8_butpressed === true) {
        localStorage.removeItem("save_balance_AR1_p8");
        window.location.reload(); //refresh page
    } else if (viewpac9_butpressed === true) {
        localStorage.removeItem("save_balance_AR1_p9");
        window.location.reload(); //refresh page
    } else if (viewpac10_butpressed === true) {
        localStorage.removeItem("save_balance_AR1_p10");
        window.location.reload(); //refresh page
    }
}


function delete_ritdiu() {
    if (viewpac1_butpressed === true) {
        localStorage.removeItem("save_ritmodiu_p1");
        window.location.reload(); //refresh page
    } else if (viewpac2_butpressed === true) {
        localStorage.removeItem("save_ritmodiu_p2");
        window.location.reload(); //refresh page
    } else if (viewpac3_butpressed === true) {
        localStorage.removeItem("save_ritmodiu_p3");
        window.location.reload(); //refresh page
    } else if (viewpac4_butpressed === true) {
        localStorage.removeItem("save_ritmodiu_p4");
        window.location.reload(); //refresh page
    } else if (viewpac5_butpressed === true) {
        localStorage.removeItem("save_ritmodiu_p5");
        window.location.reload(); //refresh page
    } else if (viewpac6_butpressed === true) {
        localStorage.removeItem("save_ritmodiu_p6");
        window.location.reload(); //refresh page
    } else if (viewpac7_butpressed === true) {
        localStorage.removeItem("save_ritmodiu_p7");
        window.location.reload(); //refresh page
    } else if (viewpac8_butpressed === true) {
        localStorage.removeItem("save_ritmodiu_p8");
        window.location.reload(); //refresh page
    } else if (viewpac9_butpressed === true) {
        localStorage.removeItem("save_ritmodiu_p9");
        window.location.reload(); //refresh page
    } else if (viewpac10_butpressed === true) {
        localStorage.removeItem("save_ritmodiu_p10");
        window.location.reload(); //refresh page
    } 
}

function delete_hemodinamico() {
    if (viewpac1_butpressed === true) {
        localStorage.removeItem("saved_cont_hemFC1");
        localStorage.removeItem("saved_cont_hemTAS1");
        localStorage.removeItem("saved_cont_hemTAD1");
        localStorage.removeItem("saved_cont_hemTAM1");
        window.location.reload(); //refresh page
    }
}